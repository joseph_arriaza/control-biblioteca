package org.carloscarrera.utilidad;

import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.TableColumn.CellEditEvent;


import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;

import javafx.scene.layout.GridPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.MenuItem;

import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Tab;
import javafx.scene.control.Button;


import java.util.ArrayList;

import org.carloscarrera.manejadores.ManejadorVarSubCategoria;
import org.carloscarrera.manejadores.ManejadorSubCategoria;
import org.carloscarrera.manejadores.ManejadorEditorial;
import org.carloscarrera.manejadores.ManejadorCategoria;
import org.carloscarrera.manejadores.ManejadorPrestamo;
import org.carloscarrera.manejadores.ManejadorUsuario;
import org.carloscarrera.manejadores.ManejadorAutor;
import org.carloscarrera.manejadores.ManejadorLibro;
import org.carloscarrera.manejadores.ManejadorRol;
import org.carloscarrera.bean.VarSubCategoria;
import org.carloscarrera.bean.SubCategoria;
import org.carloscarrera.bean.Categoria;
import org.carloscarrera.bean.Editorial;
import org.carloscarrera.bean.Prestamo;
import org.carloscarrera.bean.Usuario;
import org.carloscarrera.bean.Autor;
import org.carloscarrera.bean.Libro;
import org.carloscarrera.bean.Rol;
import org.carloscarrera.conexion.Conexion;

public class AgregarTableView implements EventHandler<Event>{
	private Label lblErr;
	private GridPane gPanelPrincipal,gPanelEditar,gPanelEdit;
	private Button editar,aceptarEdit,eliminarU,actualizarU,agregarU,cancel,agregarUs;//usuario
	private Button editarE,aceptarE,eliminarE,actualizarE,agregarE,agregarEn;//editorial
	private Button agregarL,eliminarL,buscarEpaL,aceptarL,buscarEdit,buscarCat,aceptarCat,aceptarEd;//libro
	private Button aceptarEditA,aceptarA,agregarA,eliminarA,editarA,actualizarA,buscarLA,aceptarLA;//Autor
	private Button agregarC,aceptarC,editarC,aceptarEditarC,eliminarC,actualizarC;//categoria
	private Button agregarSubC,aceptarSubC,editarSubC,aceptarEditarSubC,eliminarSubC,actualizarSubC,agregarAL,aceptarAgregarAL;//subcategoria
	private Button agregarABaceptar,buscarBook,agregarALaceptar,btnEditarSub,seeDetails,buscarSubCat;
	private Button entregado;
	private TextField txtUsername;
	private TextField txtNombre;
	private TextField txtNombreL,txtEdicion,txtPaginas,txtCategoria,txtEditorial,txtSubCat;
	private TextField txtNombreA,txtLibroA;
	private TextField txtSubCategoriaB,txtSubCategoriaL,txtSubCategoriaSubC;
	private TextField txtSubCategoria,txtSubCategoria2;
	private TextField buscarU,buscarE,buscarL,buscarA,buscarC,buscarS;
	private Label lblnombreH,lblcanitdadH,lblContra,lblContra2,lblUsername,lblRol;
	private Label lblnombreE;
	private Label lblNombreL,lblEdicion,lblPaginas,lblEditorial,lblCategoria;
	private Label lblNombreA,lblLibroA;
	private Label lblNombreC,lblNombreSC;
	private BorderPane bpPrincipal;
	private Usuario usuarioSe;
	private Categoria categoriaSel,categoriaSe;
	private Libro libroSel,libroSe,libroSelect;
	private Autor autorSe,autorEditar;
	private SubCategoria subCatSe,subCategorialSel,subcategoriaSelect;
	private Editorial editorialSe,editorialSel;
	private TableView<Usuario> tvUsuario;
	private TableView<Editorial> tvEditorial;
	private TableView<Libro> tvLibro;
	private TableView<Categoria> tvCategoria;
	private TableView<Autor> tvAutor;
	private TableView<SubCategoria>tvSubCategoria;
	private TableView<Prestamo> tvPrestamo;
	private ManejadorUsuario mUsuario;
	private ManejadorEditorial mEditorial;
	private ManejadorCategoria mCategoria;
	private ManejadorPrestamo mPrestamo;
	private int idioma;
	private TableColumn columaNombre,columnaPass,columnaRol,columaNombreE,columnaNombreL,columnaEdicion,columnaPaginas,columnaCategoria,columnaEditorial;
	private TableColumn columnaCategoriaN,columnaNombreA,columnaNombreLA;
	private TableColumn columnaSubCategoriaN;
	private SplitMenuButton selectIdioma,selectRol;
	private PasswordField pfContra,pfContra2;
	private ManejadorRol mRol;
	private ManejadorLibro mLibro;
	private ManejadorAutor mAutor;
	private ManejadorSubCategoria mSubCategoria;
	private ManejadorVarSubCategoria mVarSub;
	private Conexion conexion;
	public BorderPane agregarTable(String tablaAgregar,ManejadorUsuario mUssuario, int idiomas,Conexion conex){
		conexion=conex;
		idioma=idiomas;
		mUsuario=mUssuario;
		bpPrincipal=new BorderPane();
		gPanelPrincipal=new GridPane();
		lblErr=new Label();
		if(idioma==1){
			columaNombre = new TableColumn("USUARIO");  
			columnaPass = new TableColumn("CONTRASENIA"); 
			eliminarU=new Button("Elimiar");
			actualizarU=new Button("Actualizar");
			editar=new Button("Editar");	
			agregarU=new Button("Agregar");	
			cancel=new Button("Cancelar");				
			lblContra=new Label("Contrasenia");
			lblContra2=new Label("Repite la contrasenia");
			aceptarEdit=new Button("Editar");
			lblContra=new Label("Contrasenia");
			lblContra2=new Label("Repite la contrasenia");					
			agregarUs=new Button("Aceptar");
			//editorial
			agregarEn=new Button("Aceptar");
			eliminarE=new Button("Elimiar");
			actualizarE=new Button("Actualizar");
			editarE=new Button("Editar");	
			agregarE=new Button("Agregar");	
			cancel=new Button("Cancelar");	
			aceptarE=new Button("Aceptar");
			lblErr.setText("Falta rellenar algun dato");
			//libro
			agregarL=new Button("Agregar");
			eliminarL=new Button("Eliminar");
			buscarEpaL=new Button("Editorial");
			aceptarL=new Button("Agregar");
			buscarEdit=new Button("Editorial");
			buscarCat=new Button("Categoria");
			aceptarEd=new Button("Aceptar");
			aceptarCat=new Button("Aceptar");
			seeDetails=new Button("Detalles");
			lblNombreL=new Label("Libro");
			lblEdicion=new Label("Edicion");
			lblPaginas=new Label("Paginas");
			lblEditorial=new Label("Editorial");
			lblCategoria=new Label("Categoria");
			//autor
			agregarA=new Button("Agregar");
			eliminarA=new Button("Eliminar");
			editarA=new Button("Editar");
			actualizarA=new Button("Actualizar");
			buscarLA=new Button("Libro");
			aceptarLA=new Button("Aceptar");
			aceptarA=new Button("Agregar");
			aceptarEditA=new Button("Editar");
			lblNombreA=new Label("Autor");
			lblLibroA=new Label("Libro");
			//categoria
			agregarC=new Button("Agregar");
			aceptarC=new Button("Agregar");
			editarC=new Button("Editar");
			aceptarEditarC=new Button("Editar");
			eliminarC=new Button("Eliminar");
			actualizarC=new Button("Actualizar");
			lblNombreC=new Label("Categoria");
			//subcategoria
			agregarSubC=new Button("Agregar");
			aceptarSubC=new Button("Agregar");
			editarSubC=new Button("Editar");
			aceptarEditarSubC=new Button("Editar");
			eliminarSubC=new Button("Eliminar");
			actualizarSubC=new Button("Actualizar");
			buscarSubCat=new Button("Escoger SubCategoria");
			aceptarAgregarAL=new Button("Aceptar");
			buscarBook=new Button("Libro");
			agregarABaceptar=new Button("Aceptar");
			lblNombreSC=new Label("SubCategoria");	
			agregarALaceptar=new Button("Agregar");	
			btnEditarSub=new Button("Editar");		
			//prestamo
			entregado=new Button("Entregado");		
		}else{
			columaNombre = new TableColumn("USER");  
			columnaPass = new TableColumn("PASSWORD");  
			actualizarU=new Button("Refresh");
			eliminarU=new Button("Delete");
			editar=new Button("Edit");	
			agregarU=new Button("Add");
			cancel=new Button("Cancel");
			lblContra=new Label("Password");
			lblContra2=new Label("Repeat your password");
			aceptarEdit=new Button("Edit");
			lblContra=new Label("Password");
			lblContra2=new Label("Repeat your password");
			agregarUs=new Button("Aceptar");	
			//editorial
			agregarEn=new Button("Accept");
			actualizarE=new Button("Refresh");
			eliminarE=new Button("Delete");
			editarE=new Button("Edit");	
			agregarE=new Button("Add");
			cancel=new Button("Cancel");
			aceptarE=new Button("Accept");
			lblErr.setText("You need put the Editorial Name");
			//libro
			agregarL=new Button("Add");
			eliminarL=new Button("Delete");
			buscarEpaL=new Button("Editorial");
			aceptarL=new Button("Add");
			buscarEdit=new Button("Editorial");
			buscarCat=new Button("Category");
			aceptarEd=new Button("Accept");
			aceptarCat=new Button("Accept");
			seeDetails=new Button("Detail");
			lblNombreL=new Label("Book");
			lblEdicion=new Label("Edition");
			lblPaginas=new Label("Pages");
			lblEditorial=new Label("Editorial");
			lblCategoria=new Label("Category");
			//autor
			agregarA=new Button("Add");
			eliminarA=new Button("Delete");
			editarA=new Button("Edit");
			actualizarA=new Button("Refresh");
			buscarLA=new Button("Book");
			aceptarLA=new Button("Accept");
			aceptarA=new Button("Add");
			lblNombreA=new Label("Author");
			lblLibroA=new Label("Book");
			aceptarEditA=new Button("Edit");
			//categoria
			agregarC=new Button("Add");
			aceptarC=new Button("Add");
			editarC=new Button("Edit");
			aceptarEditarC=new Button("Edit");
			eliminarC=new Button("Delete");
			actualizarC=new Button("Refresh");
			lblNombreC=new Label("Category");
			//subcategoria
			agregarSubC=new Button("Add");
			aceptarSubC=new Button("Add");
			editarSubC=new Button("Edit");
			aceptarEditarSubC=new Button("Edit");
			eliminarSubC=new Button("Delete");
			actualizarSubC=new Button("Refresh");
			buscarSubCat=new Button("Select Sub-Category");
			aceptarAgregarAL=new Button("Accept");
			buscarBook=new Button("Book");
			agregarABaceptar=new Button("Accept");
			lblNombreSC=new Label("SubCategoria");
			agregarALaceptar=new Button("Add");
			btnEditarSub=new Button("Edit");
			//prestamo
			entregado=new Button("Commited");	
		}		
		if(tablaAgregar.equals("usuario")){					
			tvUsuario = new TableView<Usuario>(mUssuario.getListaUsuarios());
			buscarU=new TextField();
			buscarU.setPrefWidth(150);
			bpPrincipal.setTop(buscarU);
			buscarU.addEventHandler(KeyEvent.KEY_PRESSED,this);
			buscarU.setPromptText("BUSCAR");				
			editar.addEventHandler(ActionEvent.ACTION,this);			
			eliminarU.addEventHandler(ActionEvent.ACTION,this);
			actualizarU.addEventHandler(ActionEvent.ACTION,this);
			agregarU.addEventHandler(ActionEvent.ACTION,this);
	    	columaNombre.setCellValueFactory(new PropertyValueFactory<Usuario,String>("nombreUsuario"));
			columaNombre.setPrefWidth(100);				
	    	columnaPass.setCellValueFactory(new PropertyValueFactory<Usuario,String>("contrasenia"));
			columnaPass.setPrefWidth(100);
			columnaRol = new TableColumn("ROL");  
	    	columnaRol.setCellValueFactory(new PropertyValueFactory<Usuario,String>("nombreRol"));
			columnaRol.setPrefWidth(100);
			tvUsuario.getColumns().setAll(columaNombre,columnaPass,columnaRol);
			tvUsuario.setPrefWidth(302);
			gPanelPrincipal.add(tvUsuario,0,0,2,25);
			gPanelPrincipal.add(agregarU,3,1);
			gPanelPrincipal.add(editar,3,2);
			gPanelPrincipal.add(eliminarU,3,3);
			gPanelPrincipal.add(actualizarU,3,4);
			gPanelPrincipal.setVgap(8);
			gPanelPrincipal.setHgap(8);
			tvUsuario.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
			bpPrincipal.setLeft(gPanelPrincipal);
		}else if(tablaAgregar.equals("editorial")){			
			mEditorial=new ManejadorEditorial(conexion);
			tvEditorial = new TableView<Editorial>(mEditorial.getListaEditorial());	
			buscarE=new TextField();
			buscarE.setPrefWidth(150);
			bpPrincipal.setTop(buscarE);
			buscarE.addEventHandler(KeyEvent.KEY_PRESSED,this);						
			editarE.addEventHandler(ActionEvent.ACTION,this);			
			eliminarE.addEventHandler(ActionEvent.ACTION,this);
			actualizarE.addEventHandler(ActionEvent.ACTION,this);
			agregarE.addEventHandler(ActionEvent.ACTION,this);
			columaNombreE = new TableColumn("EDITORIAL"); 
			buscarE.setPromptText("BUSCAR");
	    	columaNombreE.setCellValueFactory(new PropertyValueFactory<Editorial,String>("nombre"));
			columaNombreE.setPrefWidth(175);
			tvEditorial.getColumns().add(columaNombreE);				
			gPanelPrincipal.add(tvEditorial,0,0,2,25);
			gPanelPrincipal.add(agregarE,3,1);
			gPanelPrincipal.add(editarE,3,2);
			gPanelPrincipal.add(eliminarE,3,3);
			gPanelPrincipal.add(actualizarE,3,4);
			gPanelPrincipal.setVgap(8);
			gPanelPrincipal.setHgap(8);
			tvEditorial.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
			tvEditorial.setPrefWidth(180);
			bpPrincipal.setLeft(gPanelPrincipal);
		}else if(tablaAgregar.equals("libros")){
			mLibro=new ManejadorLibro(conexion);			
			tvLibro = new TableView<Libro>(mLibro.getListaLibro());	
			buscarL=new TextField();
			buscarL.setPrefWidth(150);
			bpPrincipal.setTop(buscarL);
			buscarL.addEventHandler(KeyEvent.KEY_PRESSED,this);	
			agregarL.addEventHandler(ActionEvent.ACTION,this);
			eliminarL.addEventHandler(ActionEvent.ACTION,this);
			seeDetails.addEventHandler(ActionEvent.ACTION,this);
			buscarL.setPromptText("BUSCAR");
			columnaEditorial=new TableColumn("EDITORIAL");
			columnaEditorial.setCellValueFactory(new PropertyValueFactory<Libro,String>("nombreEditorial"));
			columnaEditorial.setPrefWidth(100);
			columnaNombreL=new TableColumn("LIBRO");	
			columnaNombreL.setCellValueFactory(new PropertyValueFactory<Libro,String>("nombreLibro"));
			columnaNombreL.setPrefWidth(100);	
			columnaCategoria=new TableColumn("CATEGORIA");
			columnaCategoria.setCellValueFactory(new PropertyValueFactory<Libro,String>("nombreCategoria"));
			columnaCategoria.setPrefWidth(75);		
			columnaEdicion=new TableColumn("EDICION");
			columnaEdicion.setCellValueFactory(new PropertyValueFactory<Libro,String>("edicion"));
			columnaEdicion.setPrefWidth(65);	
			columnaPaginas=new TableColumn("PAGINAS");
			columnaPaginas.setCellValueFactory(new PropertyValueFactory<Libro,String>("paginasLibro"));
			columnaPaginas.setPrefWidth(65);
			tvLibro.getColumns().addAll(columnaEditorial,columnaNombreL,columnaCategoria,columnaEdicion,columnaPaginas);	
			tvLibro.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
			tvLibro.setPrefWidth(407);
			gPanelPrincipal.getChildren().clear();
			gPanelPrincipal.add(tvLibro,0,0,2,25);
			gPanelPrincipal.add(agregarL,3,1);
			gPanelPrincipal.add(eliminarL,3,2);
			gPanelPrincipal.add(seeDetails,3,3);
			gPanelPrincipal.setVgap(8);
			gPanelPrincipal.setHgap(8);
			bpPrincipal.setLeft(gPanelPrincipal);
		}else if(tablaAgregar.equals("autor")){
			mAutor=new ManejadorAutor(conexion);
			buscarA=new TextField();
			buscarA.setPrefWidth(150);
			bpPrincipal.setTop(buscarA);
			buscarA.addEventHandler(KeyEvent.KEY_PRESSED,this);	
			tvAutor=new TableView<Autor>(mAutor.getListaAutor());
			editarA.addEventHandler(ActionEvent.ACTION,this);			
			eliminarA.addEventHandler(ActionEvent.ACTION,this);
			actualizarA.addEventHandler(ActionEvent.ACTION,this);
			buscarA.setPromptText("BUSCAR");
			agregarA.addEventHandler(ActionEvent.ACTION,this);
			columnaNombreA=new TableColumn("AUTOR");
			columnaNombreA.setCellValueFactory(new PropertyValueFactory<Autor,String>("nombre"));
			columnaNombreA.setPrefWidth(100);
			columnaNombreLA=new TableColumn("LIBRO");
			columnaNombreLA.setCellValueFactory(new PropertyValueFactory<Autor,String>("nombreLibro"));
			columnaNombreLA.setPrefWidth(100);
			tvAutor.getColumns().addAll(columnaNombreA,columnaNombreLA);
			tvAutor.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
			tvAutor.setPrefWidth(202);
			gPanelPrincipal.add(tvAutor,0,0,2,25);
			gPanelPrincipal.add(agregarA,3,1);
			gPanelPrincipal.add(editarA,3,2);
			gPanelPrincipal.add(eliminarA,3,3);
			gPanelPrincipal.add(actualizarA,3,4);
			gPanelPrincipal.setVgap(8);
			gPanelPrincipal.setHgap(8);
			bpPrincipal.setLeft(gPanelPrincipal);
		}else if(tablaAgregar.equals("cateogria")){
			mCategoria=new ManejadorCategoria(conexion);
			tvCategoria=new TableView<Categoria>(mCategoria.getListaCategoria());
			buscarC=new TextField();
			buscarC.setPrefWidth(150);
			bpPrincipal.setTop(buscarC);
			buscarC.addEventHandler(KeyEvent.KEY_PRESSED,this);	
			editarC.addEventHandler(ActionEvent.ACTION,this);			
			eliminarC.addEventHandler(ActionEvent.ACTION,this);
			actualizarC.addEventHandler(ActionEvent.ACTION,this);
			buscarC.setPromptText("BUSCAR");
			agregarC.addEventHandler(ActionEvent.ACTION,this);
			columnaCategoriaN=new TableColumn("CATEGORIA");
			columnaCategoriaN.setCellValueFactory(new PropertyValueFactory<Categoria,String>("nombre"));
			columnaCategoriaN.setPrefWidth(100);
			tvCategoria.getColumns().addAll(columnaCategoriaN);
			tvCategoria.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
			tvCategoria.setPrefWidth(102);
			gPanelPrincipal.add(tvCategoria,0,0,2,25);
			gPanelPrincipal.add(agregarC,3,1);
			gPanelPrincipal.add(editarC,3,2);
			gPanelPrincipal.add(eliminarC,3,3);
			gPanelPrincipal.add(actualizarC,3,4);
			gPanelPrincipal.setVgap(8);
			gPanelPrincipal.setHgap(8);
			bpPrincipal.setLeft(gPanelPrincipal);
		}else if(tablaAgregar.equals("prestamo")){
			mPrestamo=new ManejadorPrestamo(conexion);
			entregado.addEventHandler(ActionEvent.ACTION,this);
			tvPrestamo=new TableView<Prestamo>(mPrestamo.getListaPrestamo());
			TableColumn cliente=new TableColumn("CLIENTE");
			cliente.setCellValueFactory(new PropertyValueFactory<Prestamo,String>("nombre"));
			cliente.setPrefWidth(100);
			TableColumn nombreLibro=new TableColumn("LIBRO");
			nombreLibro.setCellValueFactory(new PropertyValueFactory<Prestamo,String>("nombreLibro"));
			nombreLibro.setPrefWidth(100);
			TableColumn fecha=new TableColumn("FECHA");
			fecha.setCellValueFactory(new PropertyValueFactory<Prestamo,String>("fecha"));
			fecha.setPrefWidth(100);
			TableColumn costo=new TableColumn("COSTO");
			costo.setCellValueFactory(new PropertyValueFactory<Prestamo,String>("costo"));
			costo.setPrefWidth(70);
			TableColumn estado=new TableColumn("ESTADO");
			estado.setCellValueFactory(new PropertyValueFactory<Prestamo,String>("estado"));
			estado.setPrefWidth(70);
			tvPrestamo.getColumns().addAll(cliente,nombreLibro,fecha,costo,estado);
			tvPrestamo.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
			tvPrestamo.setPrefWidth(442);			
			gPanelPrincipal.add(tvPrestamo,0,0);
			gPanelPrincipal.add(entregado,3,1);
			gPanelPrincipal.setVgap(8);
			gPanelPrincipal.setHgap(8);
			bpPrincipal.setCenter(gPanelPrincipal);
		}else if(tablaAgregar.equals("sub")){			 
			editarSubC.addEventHandler(ActionEvent.ACTION,this);			
			eliminarSubC.addEventHandler(ActionEvent.ACTION,this);
			actualizarSubC.addEventHandler(ActionEvent.ACTION,this);
			agregarSubC.addEventHandler(ActionEvent.ACTION,this);
			mSubCategoria=new ManejadorSubCategoria(conexion);
			tvSubCategoria=new TableView<SubCategoria>(mSubCategoria.getListaSubCategoria());
			buscarS=new TextField();
			buscarS.setPrefWidth(150);
			buscarS.setPromptText("BUSCAR");
			bpPrincipal.setTop(buscarS);
			buscarS.addEventHandler(KeyEvent.KEY_PRESSED,this);	
			columnaSubCategoriaN=new TableColumn("SUB-CATEGORIA");
			columnaSubCategoriaN.setCellValueFactory(new PropertyValueFactory<Prestamo,String>("nombre"));
			columnaSubCategoriaN.setPrefWidth(100);
			tvSubCategoria.setPrefWidth(102);
			tvSubCategoria.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
			tvSubCategoria.getColumns().addAll(columnaSubCategoriaN);
			gPanelPrincipal.add(tvSubCategoria,0,0,2,25);
			gPanelPrincipal.add(agregarSubC,3,1);
			gPanelPrincipal.add(editarSubC,3,2);
			gPanelPrincipal.add(eliminarSubC,3,3);
			gPanelPrincipal.add(actualizarSubC,3,4);			
			gPanelPrincipal.setVgap(8);
			gPanelPrincipal.setHgap(8);
			bpPrincipal.setLeft(gPanelPrincipal);
		}
		return bpPrincipal;
	}
	public void handle(Event event){
		if(event instanceof ActionEvent){
			if(event.getSource()==editar){
				usuarioSe = (Usuario)tvUsuario.getSelectionModel().getSelectedItem();
				if(usuarioSe!=null){
					gPanelEdit=new GridPane();					
					aceptarEdit.addEventHandler(ActionEvent.ACTION,this);
					cancel.addEventHandler(ActionEvent.ACTION,this);
					pfContra=new PasswordField();
					pfContra2=new PasswordField();
					gPanelEdit.getChildren().clear();
					try{
						gPanelEdit.add(lblContra,2,0);
						gPanelEdit.add(pfContra,2,1);
						gPanelEdit.add(lblContra2,2,2);
						gPanelEdit.add(pfContra2,2,3);
						gPanelEdit.add(aceptarEdit,2,4);
						gPanelEdit.add(cancel,3,4);
						gPanelEdit.setVgap(8);
						gPanelEdit.setHgap(8);
						bpPrincipal.setCenter(gPanelEdit);
					}catch(IllegalArgumentException ieea){

					}
				}
			}else if(event.getSource()==eliminarU){
				Usuario usuarioSel = (Usuario)tvUsuario.getSelectionModel().getSelectedItem();
				if(usuarioSel!=null){
					mUsuario.eliminarUsuario(usuarioSel.getIdUsuario());
				}
			}else if(event.getSource()==actualizarU){
				mUsuario.actualizarLista();
			}else if(event.getSource()==aceptarEdit){
				if(!pfContra2.getText().trim().equals("") && !pfContra.getText().trim().equals("")){
					if(pfContra.getText().equals(pfContra2.getText())){
						mUsuario.actualizarUsuario(pfContra.getText(),usuarioSe.getIdUsuario());
						bpPrincipal.setCenter(null);
					}else{
						if(idioma==1){
							lblErr.setText("Las contrasenias no son iguales");
						}else{
							lblErr.setText("The password is wrong");
						}
					}
				}else{
					if(idioma==1){
						lblErr.setText("Falta rellenar algun dato");
					}else{
						lblErr.setText("Fill the passwordfields.");
					}
					try{
						gPanelEdit.add(lblErr,1,5,3,3);
					}catch(IllegalArgumentException iea){

					}
				}
			}else if(event.getSource()==cancel){
				bpPrincipal.setCenter(null);
			}else if(event.getSource()==agregarU){
				txtUsername=new TextField("");
				pfContra=new PasswordField();
				pfContra2=new PasswordField();
				lblUsername=new Label("Username");
				lblRol=new Label("Rol");
				selectRol=new SplitMenuButton();
				selectRol.setText("Rol");				
				agregarUs.addEventHandler(ActionEvent.ACTION,this);
				selectRol.setPrefWidth(150);
				mRol=new ManejadorRol(conexion);
				ArrayList<Rol> listaRol=new ArrayList<Rol>(mRol.getListaRol());
				for(Rol rol:listaRol){
					MenuItem propiedad=new MenuItem(""+rol.getNombre());
					propiedad.setOnAction(new EventHandler<ActionEvent>() {
						@Override public void handle(ActionEvent e) {
							selectRol.setText(propiedad.getText());
						}
					});
					selectRol.getItems().add(propiedad);
				}
				cancel.addEventHandler(ActionEvent.ACTION,this);
				gPanelEdit=new GridPane();
				gPanelEdit.getChildren().clear();
				gPanelEdit.add(lblUsername,2,0);
				gPanelEdit.add(txtUsername,2,1);
				gPanelEdit.add(lblContra,2,2);
				gPanelEdit.add(pfContra,2,3,2,1);
				gPanelEdit.add(lblContra2,2,4);
				gPanelEdit.add(pfContra2,2,5,2,1);
				gPanelEdit.add(lblRol,2,6,2,1);
				gPanelEdit.add(selectRol,2,7,2,1);
				gPanelEdit.add(agregarUs,2,8);
				gPanelEdit.add(cancel,3,8);
				gPanelEdit.setVgap(8);
				gPanelEdit.setHgap(8);
				bpPrincipal.setCenter(gPanelEdit);
				try{
					bpPrincipal.setCenter(gPanelEdit);
				}catch(IllegalArgumentException ieal){

				}
			}else if(event.getSource()==agregarUs){
				if(!selectRol.getText().equals("Rol") && !txtUsername.getText().trim().equals("") && !pfContra2.getText().trim().equals("") && !pfContra.getText().trim().equals("")){
					if(pfContra.getText().equals(pfContra2.getText())){
						ArrayList<Usuario> listaUsers=new ArrayList<Usuario>(mUsuario.getListaUsuarios());
						boolean existenten=false;
						for(Usuario user:listaUsers){
							if(user.getNombreUsuario().equals(txtUsername.getText())){
								existenten=true;
							}
						}
						if(existenten){
							if(idioma==1){
								lblErr.setText("Usuario elegido por otro miembro");
							}else{
								lblErr.setText("This username has taken");
							}
						}else{
							Usuario userAdd = new Usuario();
							userAdd.setContrasenia(pfContra.getText());
							userAdd.setNombreUsuario(txtUsername.getText());
							if(selectRol.getText().equalsIgnoreCase("administrador")){
								userAdd.setIdRol(1);
							}else if(selectRol.getText().equalsIgnoreCase("editor")){
								userAdd.setIdRol(2);
							}else if(selectRol.getText().equalsIgnoreCase("visualizador de reporte")){
								userAdd.setIdRol(3);
							}else if(selectRol.getText().equalsIgnoreCase("alumno")){
								userAdd.setIdRol(4);
							}
							mUsuario.agregarUsuario(userAdd);
							bpPrincipal.setCenter(null);
						}					
					}else{
						if(idioma==1){
							lblErr.setText("Las contrasenias no son iguales");
						}else{
							lblErr.setText("The password is wrong");
						}
					}
				}else{
					if(idioma==1){
						lblErr.setText("Falta rellenar algun dato");
					}else{
						lblErr.setText("Fill the fields.");
					}			
				}
				try{
					gPanelEdit.add(lblErr,1,9,3,3);
				}catch(IllegalArgumentException iea){

				}
			}else if(event.getSource()==editarE){				
				editorialSe=(Editorial) tvEditorial.getSelectionModel().getSelectedItem();
				if(editorialSe!=null){
					gPanelEdit=new GridPane();
					txtNombre=new TextField("");
					lblnombreE=new Label("Editorial");
					
					aceptarE.addEventHandler(ActionEvent.ACTION,this);
					cancel.addEventHandler(ActionEvent.ACTION,this);
					gPanelEdit.getChildren().clear();
					try{
						gPanelEdit.add(lblnombreE,2,0,2,1);
						gPanelEdit.add(txtNombre,2,1,2,1);
						gPanelEdit.add(aceptarE,2,2);
						gPanelEdit.add(cancel,3,2);
						gPanelEdit.setVgap(8);
						gPanelEdit.setHgap(8);
						bpPrincipal.setCenter(gPanelEdit);
					}catch(IllegalArgumentException ieea){

					}
				}

			}else if(event.getSource()==aceptarE){
				if(!txtNombre.getText().trim().equals("")){
					mEditorial.actualizarEditorial(txtNombre.getText(),editorialSe.getIdEditorial());
					bpPrincipal.setCenter(null);
				}else{
					if(idioma==1){
						lblErr.setText("Falta rellenar algun dato");
					}else{
						lblErr.setText("You need put the Editorial Name");
					}
					try{
						gPanelEdit.add(lblErr,1,3);
					}catch(IllegalArgumentException ieaa){

					}
				}
			}else if(event.getSource()==agregarE){
				gPanelEdit=new GridPane();
				txtNombre=new TextField("");
				lblnombreE=new Label("Editorial");				
				agregarEn.addEventHandler(ActionEvent.ACTION,this);
				cancel.addEventHandler(ActionEvent.ACTION,this);
				gPanelEdit.getChildren().clear();
				try{
					gPanelEdit.add(lblnombreE,2,0,2,1);
					gPanelEdit.add(txtNombre,2,1,2,1);
					gPanelEdit.add(agregarEn,2,2);
					gPanelEdit.add(cancel,3,2);
					gPanelEdit.setVgap(8);
					gPanelEdit.setHgap(8);
					bpPrincipal.setCenter(gPanelEdit);
				}catch(IllegalArgumentException ieea){
				}
			}else if(event.getSource()==agregarEn){
				if(!txtNombre.getText().trim().equals("")){
					mEditorial.agregarEditorial(txtNombre.getText());
					bpPrincipal.setCenter(null);
				}else{					
					try{
						if(idioma==1){
							lblErr.setText("Falta rellenar algun dato");
						}else{
							lblErr.setText("Fill all Fields");
						}
						gPanelEdit.add(lblErr,3,6,2,1);
					}catch(IllegalArgumentException ieaa){

					}
				}
			}else if(event.getSource()==eliminarE){
				Editorial editorialElim=(Editorial)tvEditorial.getSelectionModel().getSelectedItem();
				if(editorialElim!=null){
					mEditorial.eliminarEditorial(editorialElim.getIdEditorial());
				}
			}else if(event.getSource()==agregarL){
				gPanelEdit=new GridPane();
				txtNombreL=new TextField();
				txtPaginas=new TextField();
				txtEdicion=new TextField();
				txtEditorial=new TextField();
				txtEditorial.setEditable(false);
				txtCategoria=new TextField();
				txtCategoria.setEditable(false);
				txtSubCat=new TextField();
				txtSubCat.setEditable(false);

				gPanelEdit.getChildren().clear();
				cancel.addEventHandler(ActionEvent.ACTION,this);
				gPanelEdit.setVgap(5);
				gPanelEdit.setHgap(5);
				buscarEdit.addEventHandler(ActionEvent.ACTION,this);
				buscarCat.addEventHandler(ActionEvent.ACTION,this);
				aceptarL.addEventHandler(ActionEvent.ACTION,this);
				buscarSubCat.addEventHandler(ActionEvent.ACTION,this);
				try{
					gPanelEdit.add(lblNombreL,2,0,2,1);
					gPanelEdit.add(txtNombreL,2,1,2,1);
					gPanelEdit.add(lblPaginas,2,2,2,1);
					gPanelEdit.add(txtPaginas,2,3,2,1);
					gPanelEdit.add(lblEdicion,2,4,2,1);
					gPanelEdit.add(txtEdicion,2,5,2,1);
					gPanelEdit.add(lblEditorial,2,6,2,1);
					gPanelEdit.add(txtEditorial,2,7,2,1);
					gPanelEdit.add(buscarEdit,4,7);
					gPanelEdit.add(lblCategoria,2,8,2,1);
					gPanelEdit.add(txtCategoria,2,9,2,1);
					gPanelEdit.add(buscarCat,4,9);
					gPanelEdit.add(lblNombreSC,2,10,2,1);
					gPanelEdit.add(txtSubCat,2,11,2,1);
					gPanelEdit.add(buscarSubCat,4,11,2,1);					
					gPanelEdit.add(aceptarL,2,12);
					gPanelEdit.add(cancel,3,12);
					bpPrincipal.setCenter(gPanelEdit);
				}catch(IllegalArgumentException illea){

				}
			}else if(event.getSource()==buscarEdit){
				mEditorial=new ManejadorEditorial(conexion);
				tvEditorial = new TableView<Editorial>(mEditorial.getListaEditorial());			
				columaNombreE = new TableColumn("EDITORIAL"); 
				aceptarEd.addEventHandler(ActionEvent.ACTION,this);
		    	columaNombreE.setCellValueFactory(new PropertyValueFactory<Editorial,String>("nombre"));
				columaNombreE.setPrefWidth(175);
				tvEditorial.setPrefWidth(178);
				tvEditorial.getColumns().add(columaNombreE);
				gPanelEdit.getChildren().clear();
				gPanelEdit.add(tvEditorial,0,0,2,25);
				gPanelEdit.add(aceptarEd,3,1);
			}else if(event.getSource()==aceptarEd){
				editorialSel=(Editorial) tvEditorial.getSelectionModel().getSelectedItem();
				if(editorialSel!=null){
					gPanelEdit.getChildren().clear();
					txtEditorial.setText(editorialSel.getNombre());
					try{
						gPanelEdit.add(lblNombreL,2,0,2,1);
						gPanelEdit.add(txtNombreL,2,1,2,1);
						gPanelEdit.add(lblPaginas,2,2,2,1);
						gPanelEdit.add(txtPaginas,2,3,2,1);
						gPanelEdit.add(lblEdicion,2,4,2,1);
						gPanelEdit.add(txtEdicion,2,5,2,1);
						gPanelEdit.add(lblEditorial,2,6,2,1);
						gPanelEdit.add(txtEditorial,2,7,2,1);
						gPanelEdit.add(buscarEdit,4,7);
						gPanelEdit.add(lblCategoria,2,8,2,1);
						gPanelEdit.add(txtCategoria,2,9,2,1);
						gPanelEdit.add(buscarCat,4,9);
						gPanelEdit.add(lblNombreSC,2,10,2,1);
						gPanelEdit.add(txtSubCat,2,11,2,1);
						gPanelEdit.add(buscarSubCat,4,11,2,1);					
						gPanelEdit.add(aceptarL,2,12);
						gPanelEdit.add(cancel,3,12);
						bpPrincipal.setCenter(gPanelEdit);
					}catch(IllegalArgumentException illea){

					}
				}
			}else if(event.getSource()==buscarCat){
				mCategoria=new ManejadorCategoria(conexion);
				tvCategoria = new TableView<Categoria>(mCategoria.getListaCategoria());			
				columnaCategoriaN = new TableColumn("CATEGORIA"); 
		    	columnaCategoriaN.setCellValueFactory(new PropertyValueFactory<Editorial,String>("nombre"));
				columnaCategoriaN.setPrefWidth(100);
				aceptarCat.addEventHandler(ActionEvent.ACTION,this);
				tvCategoria.setPrefWidth(103);
				tvCategoria.getColumns().add(columnaCategoriaN);
				tvCategoria.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
				gPanelEdit.getChildren().clear();
				gPanelEdit.add(tvCategoria,0,0,2,25);
				gPanelEdit.add(aceptarCat,3,1);
			}else if(event.getSource()==aceptarCat){
				categoriaSel=(Categoria) tvCategoria.getSelectionModel().getSelectedItem();
				if(categoriaSel!=null){
					gPanelEdit.getChildren().clear();
					txtCategoria.setText(categoriaSel.getNombre());
					try{
						gPanelEdit.add(lblNombreL,2,0,2,1);
						gPanelEdit.add(txtNombreL,2,1,2,1);
						gPanelEdit.add(lblPaginas,2,2,2,1);
						gPanelEdit.add(txtPaginas,2,3,2,1);
						gPanelEdit.add(lblEdicion,2,4,2,1);
						gPanelEdit.add(txtEdicion,2,5,2,1);
						gPanelEdit.add(lblEditorial,2,6,2,1);
						gPanelEdit.add(txtEditorial,2,7,2,1);
						gPanelEdit.add(buscarEdit,4,7);
						gPanelEdit.add(lblCategoria,2,8,2,1);
						gPanelEdit.add(txtCategoria,2,9,2,1);
						gPanelEdit.add(buscarCat,4,9);
						gPanelEdit.add(lblNombreSC,2,10,2,1);
						gPanelEdit.add(txtSubCat,2,11,2,1);
						gPanelEdit.add(buscarSubCat,4,11,2,1);					
						gPanelEdit.add(aceptarL,2,12);
						gPanelEdit.add(cancel,3,12);
						bpPrincipal.setCenter(gPanelEdit);
					}catch(IllegalArgumentException illea){

					}
				}
			}else if(event.getSource()==aceptarL){
				if(!txtSubCat.getText().trim().equals("") && !txtNombreL.getText().trim().equals("") && !txtPaginas.getText().trim().equals("") && !txtEdicion.getText().trim().equals("") && !txtCategoria.getText().trim().equals("") && !txtEditorial.getText().trim().equals("")){
					try{
						mVarSub=new ManejadorVarSubCategoria(conexion);
						int paginas=Integer.parseInt(txtPaginas.getText());
						int edicion=Integer.parseInt(txtEdicion.getText());
						Libro libroAdd=new Libro();
						libroAdd.setIdCategoria(categoriaSel.getIdCategoria());
						libroAdd.setIdEditorial(editorialSel.getIdEditorial());
						libroAdd.setNombreLibro(txtNombreL.getText());
						libroAdd.setEdicion(edicion);
						libroAdd.setPaginasLibro(paginas);
						mLibro.agregarLibro(libroAdd);
						mVarSub.agregarVarSubCategoria(mLibro.lastId(),subcategoriaSelect.getIdSubCategoria());
						bpPrincipal.setCenter(null);
					}catch(NumberFormatException nfe){
						lblErr.setText("Las paginas y la edicion deben ser numeros.");
					}
				}else{
					if(idioma==1){
						lblErr.setText("Falta rellenar algun dato");
					}else{
						lblErr.setText("Fill all Fields");
					}
				}
				try{
					gPanelEdit.add(lblErr,2,15,2,1);
				}catch(IllegalArgumentException ieaa){
				}
			}else if(event.getSource()==eliminarL){
				Libro libroDel=(Libro) tvLibro.getSelectionModel().getSelectedItem();
				if(libroDel!=null){
					mLibro.eliminarLibro(libroDel.getIdLibro());
				}
			}else if(event.getSource()==actualizarE){
				mEditorial.actualizarLista();
			}else if(event.getSource()==agregarA){
				gPanelEdit=new GridPane();
				txtNombreA=new TextField();
				txtLibroA=new TextField();
				txtLibroA.setEditable(false);
				aceptarA.addEventHandler(ActionEvent.ACTION,this);
				cancel.addEventHandler(ActionEvent.ACTION,this);
				buscarLA.addEventHandler(ActionEvent.ACTION,this);
				try{
					gPanelEdit.add(lblNombreA,2,0,2,1);
					gPanelEdit.add(txtNombreA,2,1,2,1);
					gPanelEdit.add(lblLibroA,2,2,2,1);
					gPanelEdit.add(txtLibroA,2,3,2,1);
					gPanelEdit.add(buscarLA,4,3);
					gPanelEdit.add(aceptarA,2,5);
					gPanelEdit.add(cancel,4,5);
					gPanelEdit.setVgap(5);
					gPanelEdit.setHgap(5);
					bpPrincipal.setCenter(gPanelEdit);
				}catch(IllegalArgumentException illeas){
				}
			}else if(event.getSource()==buscarLA){
				aceptarLA.addEventHandler(ActionEvent.ACTION,this);
				mLibro=new ManejadorLibro(conexion);
				tvLibro = new TableView<Libro>(mLibro.getListaLibro());	
				columnaNombreL=new TableColumn("LIBRO");	
				columnaNombreL.setCellValueFactory(new PropertyValueFactory<Libro,String>("nombreLibro"));
				columnaNombreL.setPrefWidth(100);	
				tvLibro.getColumns().addAll(columnaNombreL);	
				tvLibro.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
				tvLibro.setPrefWidth(102);
				gPanelEdit.getChildren().clear();
				try{
					gPanelEdit.add(tvLibro,0,0,2,25);
					gPanelEdit.add(aceptarLA,3,1);
					bpPrincipal.setCenter(gPanelEdit);
				}catch(IllegalArgumentException illeas){
				}
			}else if(event.getSource()==aceptarLA){
				libroSel=(Libro)tvLibro.getSelectionModel().getSelectedItem();
				if(libroSel!=null){
					gPanelEdit.getChildren().clear();
					txtLibroA.setText(libroSel.getNombreLibro());
					try{
						gPanelEdit.add(lblNombreA,2,0,2,1);
						gPanelEdit.add(txtNombreA,2,1,2,1);
						gPanelEdit.add(lblLibroA,2,2,2,1);
						gPanelEdit.add(txtLibroA,2,3,2,1);
						gPanelEdit.add(buscarLA,4,3);
						gPanelEdit.add(aceptarA,1,5);
						gPanelEdit.add(cancel,3,5);
						gPanelEdit.setVgap(5);
						gPanelEdit.setHgap(5);
						bpPrincipal.setCenter(gPanelEdit);
					}catch(IllegalArgumentException illeas){

					}
				}
			}else if(event.getSource()==aceptarA){
				if(!txtNombreA.getText().trim().equals("") && !txtLibroA.getText().trim().equals("")){
					mAutor.agregarAutor(txtNombreA.getText(),libroSel.getIdLibro());	
					bpPrincipal.setCenter(null);				
				}else{
					if(idioma==1){
						lblErr.setText("Falta rellenar algun dato");
					}else{
						lblErr.setText("Fill all Fields");
					}
				}
				try{
					gPanelEdit.add(lblErr,2,7,2,1);
				}catch(IllegalArgumentException ieaa){
				}
			}else if(event.getSource()==actualizarA){
				mAutor.actualizarLista();
			}else if(event.getSource()==eliminarA){
				Autor autorDel=(Autor)tvAutor.getSelectionModel().getSelectedItem();
				if(autorDel!=null){
					mAutor.eliminarAutor(autorDel.getIdAutor());
				}
			}else if(event.getSource()==editarA){
				autorEditar=(Autor) tvAutor.getSelectionModel().getSelectedItem();
				if(autorEditar!=null){
					gPanelEdit=new GridPane();
					txtNombreA=new TextField();
					aceptarEditA.addEventHandler(ActionEvent.ACTION,this);
					cancel.addEventHandler(ActionEvent.ACTION,this);
					try{
						gPanelEdit.add(lblNombreA,2,0,2,1);
						gPanelEdit.add(txtNombreA,2,1,2,1);
						gPanelEdit.add(aceptarEditA,2,3);
						gPanelEdit.add(cancel,3,3);
						gPanelEdit.setVgap(5);
						gPanelEdit.setHgap(5);
						bpPrincipal.setCenter(gPanelEdit);
					}catch(IllegalArgumentException illeas){
					}
				}
			}else if(event.getSource()==aceptarEditA){
				if(!txtNombreA.getText().trim().equals("")){
					mAutor.actualizarAutor(autorEditar.getIdAutor(),txtNombreA.getText());	
					bpPrincipal.setCenter(null);				
				}else{
					if(idioma==1){
						lblErr.setText("Falta rellenar algun dato");
					}else{
						lblErr.setText("Fill all Fields");
					}
				}
				try{
					gPanelEdit.add(lblErr,2,4,2,1);
				}catch(IllegalArgumentException ieaa){
				}
			}else if(event.getSource()==actualizarC){
				mCategoria.actualizarLista();
			}else if(event.getSource()==agregarC){
				gPanelEdit=new GridPane();
				txtCategoria=new TextField();
				aceptarC.addEventHandler(ActionEvent.ACTION,this);
				cancel.addEventHandler(ActionEvent.ACTION,this);
				gPanelEdit.getChildren().clear();
				try{					
					gPanelEdit.add(lblNombreC,2,0,2,1);
					gPanelEdit.add(txtCategoria,2,1,2,1);
					gPanelEdit.add(aceptarC,2,3);
					gPanelEdit.add(cancel,3,3);
					gPanelEdit.setVgap(5);
					gPanelEdit.setHgap(5);
					bpPrincipal.setCenter(gPanelEdit);
				}catch(IllegalArgumentException illeas){

				}
			}else if(event.getSource()==aceptarC){
				if(!txtCategoria.getText().trim().equals("")){
					ArrayList<Categoria> listaCat=new ArrayList<Categoria>(mCategoria.getListaCategoria());
					boolean verificar=false;
					for(Categoria cat:listaCat){
						if(cat.getNombre().equalsIgnoreCase(txtCategoria.getText())){
							verificar=true;
						}
					}
					if(verificar){
						if(idioma==1){
							lblErr.setText("Ya existe una categoria con ese nombre");
						}else{
							lblErr.setText("This Category is already created");
						}
					}else{
						mCategoria.agregarCategoria(txtCategoria.getText());
						bpPrincipal.setCenter(null);
					}			
				}else{
					if(idioma==1){
						lblErr.setText("Falta rellenar algun dato");
					}else{
						lblErr.setText("Fill all Fields");
					}
				}
				try{
					gPanelEdit.add(lblErr,2,4,2,1);
				}catch(IllegalArgumentException ieaa){
				}
			}else if(event.getSource()==editarC){
				categoriaSe=(Categoria)tvCategoria.getSelectionModel().getSelectedItem();
				if(categoriaSe!=null){
					gPanelEdit=new GridPane();
					txtCategoria=new TextField();
					aceptarEditarC.addEventHandler(ActionEvent.ACTION,this);
					cancel.addEventHandler(ActionEvent.ACTION,this);
					gPanelEdit.getChildren().clear();
					try{					
						gPanelEdit.add(lblNombreC,2,0,2,1);
						gPanelEdit.add(txtCategoria,2,1,2,1);
						gPanelEdit.add(aceptarEditarC,2,3);
						gPanelEdit.add(cancel,3,3);
						gPanelEdit.setVgap(5);
						gPanelEdit.setHgap(5);
						bpPrincipal.setCenter(gPanelEdit);
					}catch(IllegalArgumentException illeas){

					}
				}
			}else if(event.getSource()==aceptarEditarC){
				if(!txtCategoria.getText().trim().equals("")){
					ArrayList<Categoria> listaCat=new ArrayList<Categoria>(mCategoria.getListaCategoria());
					boolean verificarCat=false;
					for(Categoria cat:listaCat){
						if(cat.getNombre().equalsIgnoreCase(txtCategoria.getText())){
							verificarCat=true;
						}
					}
					if(verificarCat){
						if(idioma==1){
							lblErr.setText("Ya existe una categoria con ese nombre");
						}else{
							lblErr.setText("This Category is already created");
						}
					}else{
						mCategoria.actualizarCategoria(categoriaSe.getIdCategoria(),txtCategoria.getText());
						bpPrincipal.setCenter(null);
					}			
				}else{
					if(idioma==1){
						lblErr.setText("Falta rellenar algun dato");
					}else{
						lblErr.setText("Fill all Fields");
					}
				}
				try{
					gPanelEdit.add(lblErr,2,4,2,1);
				}catch(IllegalArgumentException ieaa){
				}
			}else if(event.getSource()==actualizarSubC){
				mSubCategoria.actualizarLista();
			}else if(event.getSource()==agregarSubC){				
				gPanelEdit=new GridPane();
				txtSubCategoria=new TextField();
				aceptarSubC.addEventHandler(ActionEvent.ACTION,this);
				cancel.addEventHandler(ActionEvent.ACTION,this);
				gPanelEdit.getChildren().clear();
				try{					
					gPanelEdit.add(lblNombreSC,2,0,2,1);
					gPanelEdit.add(txtSubCategoria,2,1,2,1);
					gPanelEdit.add(aceptarSubC,2,3);
					gPanelEdit.add(cancel,3,3);
					gPanelEdit.setVgap(5);
					gPanelEdit.setHgap(5);
					bpPrincipal.setCenter(gPanelEdit);
				}catch(IllegalArgumentException illeas){

				}
			}else if(event.getSource()==aceptarSubC){
				if(!txtSubCategoria.getText().trim().equals("")){
					ArrayList<SubCategoria> listaSubCat=new ArrayList<SubCategoria>(mSubCategoria.getListaSubCategoria());
					boolean verificar=false;
					for(SubCategoria subCat:listaSubCat){
						if(subCat.getNombre().equalsIgnoreCase(txtSubCategoria.getText())){
							verificar=true;
						}
					}
					if(verificar){
						if(idioma==1){
							lblErr.setText("Ya existe una sub-categoria con ese nombre");
						}else{
							lblErr.setText("This Sub-Category is already created");
						}
					}else{
						mSubCategoria.agregarSubCategoria(txtSubCategoria.getText());
						bpPrincipal.setCenter(null);
					}			
				}else{
					if(idioma==1){
						lblErr.setText("Falta rellenar algun dato");
					}else{
						lblErr.setText("Fill all Fields");
					}
				}
				try{
					gPanelEdit.add(lblErr,2,4,2,1);
				}catch(IllegalArgumentException ieaa){
				}
			}else if(event.getSource()==buscarSubCat){
				gPanelEdit=new GridPane();
				mSubCategoria=new ManejadorSubCategoria(conexion);
				tvSubCategoria=new TableView<SubCategoria>(mSubCategoria.getListaSubCategoria());
				columnaSubCategoriaN=new TableColumn("SUB-CATEGORIA");
				agregarALaceptar.addEventHandler(ActionEvent.ACTION,this);
				columnaSubCategoriaN.setCellValueFactory(new PropertyValueFactory<Prestamo,String>("nombre"));
				columnaSubCategoriaN.setPrefWidth(100);
				tvSubCategoria.setPrefWidth(102);
				tvSubCategoria.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
				tvSubCategoria.getColumns().addAll(columnaSubCategoriaN);
				gPanelEdit.add(tvSubCategoria,0,0,2,25);		
				gPanelEdit.add(agregarALaceptar,3,3);
				gPanelEdit.setVgap(8);
				gPanelEdit.setHgap(8);
				bpPrincipal.setCenter(gPanelEdit);
			}else if(event.getSource()==editarSubC){
				subCatSe=(SubCategoria)tvSubCategoria.getSelectionModel().getSelectedItem();
				if(subCatSe!=null){
					gPanelEdit=new GridPane();
					txtSubCategoria2=new TextField();
					aceptarEditarSubC.addEventHandler(ActionEvent.ACTION,this);
					cancel.addEventHandler(ActionEvent.ACTION,this);
					gPanelEdit.getChildren().clear();
					try{					
						gPanelEdit.add(lblNombreSC,2,0,2,1);
						gPanelEdit.add(txtSubCategoria2,2,1,2,1);
						gPanelEdit.add(aceptarEditarSubC,2,3);
						gPanelEdit.add(cancel,3,3);
						gPanelEdit.setVgap(5);
						gPanelEdit.setHgap(5);
						bpPrincipal.setCenter(gPanelEdit);
					}catch(IllegalArgumentException illeas){
					}
				}
			}else if(event.getSource()==aceptarEditarSubC){
				if(!txtSubCategoria2.getText().trim().equals("")){
					ArrayList<SubCategoria> listaSubCat=new ArrayList<SubCategoria>(mSubCategoria.getListaSubCategoria());
					boolean verificar=false;
					for(SubCategoria subCat:listaSubCat){
						if(subCat.getNombre().equalsIgnoreCase(txtSubCategoria2.getText())){
							verificar=true;
						}
					}
					if(verificar){
						if(idioma==1){
							lblErr.setText("Ya existe una sub-categoria con ese nombre");
						}else{
							lblErr.setText("This Sub-Category is already created");
						}
					}else{
						mSubCategoria.actualizarSubCategoria(subCatSe.getIdSubCategoria(),txtSubCategoria2.getText());
						bpPrincipal.setCenter(null);
					}			
				}else{
					if(idioma==1){
						lblErr.setText("Falta rellenar algun dato");
					}else{
						lblErr.setText("Fill all Fields");
					}
				}
				try{
					gPanelEdit.add(lblErr,2,4,2,1);
				}catch(IllegalArgumentException ieaa){
				}
			}else if(event.getSource()==eliminarSubC){
				SubCategoria eliminiarSub=(SubCategoria)tvSubCategoria.getSelectionModel().getSelectedItem();
				if(eliminiarSub!=null){
					mSubCategoria.eliminarSubCategoria(eliminiarSub.getIdSubCategoria());
				}
			}else if(event.getSource()==agregarAL){
				subCategorialSel=(SubCategoria) tvSubCategoria.getSelectionModel().getSelectedItem();
				if(subCategorialSel!=null){
					gPanelEdit=new GridPane();
					txtSubCategoriaSubC=new TextField();
					txtSubCategoriaSubC.setEditable(false);
					txtSubCategoriaSubC.setText(subCategorialSel.getNombre());
					txtSubCategoriaL=new TextField();
					txtSubCategoriaL.setEditable(false);	
					cancel.addEventHandler(ActionEvent.ACTION,this);
					buscarBook.addEventHandler(ActionEvent.ACTION,this);
					btnEditarSub.addEventHandler(ActionEvent.ACTION,this);
					gPanelEdit.getChildren().clear();			
					try{
						gPanelEdit.add(lblNombreSC,2,0,2,1);
						gPanelEdit.add(txtSubCategoriaSubC,2,1,2,1);
						gPanelEdit.add(lblLibroA,2,2,2,1);
						gPanelEdit.add(txtSubCategoriaL,2,3,2,1);
						gPanelEdit.add(buscarBook,4,3);
						gPanelEdit.add(btnEditarSub,2,4);
						gPanelEdit.add(cancel,4,4);
						gPanelEdit.setVgap(5);
						gPanelEdit.setHgap(5);
						bpPrincipal.setCenter(gPanelEdit);
					}catch(IllegalArgumentException illeas){
					}
				}

			}else if(event.getSource()==agregarALaceptar){
				subcategoriaSelect=(SubCategoria)tvSubCategoria.getSelectionModel().getSelectedItem();
				if(subcategoriaSelect!=null){
					txtSubCat.setText(subcategoriaSelect.getNombre());
					gPanelEdit.getChildren().clear();
					try{
						gPanelEdit.add(lblNombreL,2,0,2,1);
						gPanelEdit.add(txtNombreL,2,1,2,1);
						gPanelEdit.add(lblPaginas,2,2,2,1);
						gPanelEdit.add(txtPaginas,2,3,2,1);
						gPanelEdit.add(lblEdicion,2,4,2,1);
						gPanelEdit.add(txtEdicion,2,5,2,1);
						gPanelEdit.add(lblEditorial,2,6,2,1);
						gPanelEdit.add(txtEditorial,2,7,2,1);
						gPanelEdit.add(buscarEdit,4,7);
						gPanelEdit.add(lblCategoria,2,8,2,1);
						gPanelEdit.add(txtCategoria,2,9,2,1);
						gPanelEdit.add(buscarCat,4,9);
						gPanelEdit.add(lblNombreSC,2,10,2,1);
						gPanelEdit.add(txtSubCat,2,11,2,1);
						gPanelEdit.add(buscarSubCat,4,11,2,1);					
						gPanelEdit.add(aceptarL,2,12);
						gPanelEdit.add(cancel,3,12);
						bpPrincipal.setCenter(gPanelEdit);
					}catch(IllegalArgumentException illea){

					}
				}
			}else if(event.getSource()==btnEditarSub){
				if(!txtSubCategoriaL.getText().trim().equals("") && !txtSubCategoriaSubC.getText().trim().equals("")){
					mVarSub=new ManejadorVarSubCategoria(conexion);
					ArrayList<VarSubCategoria> listaVar=new ArrayList<VarSubCategoria>(mVarSub.getListaVarSubCategoria());
					boolean verificando=false;
					for(VarSubCategoria variable:listaVar){
						if(variable.getIdLibro()==libroSelect.getIdLibro() && variable.getIdSubCategoria()==subCategorialSel.getIdSubCategoria()){
							verificando=true;
						}
					}
					if(verificando){
						if(idioma==1){
							lblErr.setText("Este libro ya tiene esta subcategoria");
						}else{
							lblErr.setText("This book already has this subcategory");
						}
					}else{
						mVarSub.agregarVarSubCategoria(libroSelect.getIdLibro(),subCategorialSel.getIdSubCategoria());
						bpPrincipal.setCenter(null);
					}
				}else{
					if(idioma==1){
						lblErr.setText("Falta rellenar algun dato");
					}else{
						lblErr.setText("Fill all Fields");
					}
				}
				try{
					gPanelEdit.add(lblErr,2,6,2,1);
				}catch(IllegalArgumentException ieaa){
				}
			}else if(event.getSource()==seeDetails){
				Libro libroSelec=(Libro) tvLibro.getSelectionModel().getSelectedItem();
				if(libroSelec!=null){
					mVarSub=new ManejadorVarSubCategoria(conexion);
					TableView<VarSubCategoria> tvVarSubCat= new TableView<VarSubCategoria>(mVarSub.getListaVarSubCategoriaLibro(libroSelec.getIdLibro()));
					TableColumn<VarSubCategoria,String> tablaNombreLibro=new TableColumn<VarSubCategoria,String>("LIBRO");
					tablaNombreLibro.setCellValueFactory(new PropertyValueFactory("nombreLibro"));
					tablaNombreLibro.setPrefWidth(100);
					TableColumn<VarSubCategoria,String> tablaCat=new TableColumn<VarSubCategoria,String>("SUB-CATEGORIA");
					tablaCat.setCellValueFactory(new PropertyValueFactory("nombreCat"));
					tablaCat.setPrefWidth(100);
					tvVarSubCat.setPrefWidth(202);
					tvVarSubCat.getColumns().addAll(tablaNombreLibro,tablaCat);
					bpPrincipal.setCenter(tvVarSubCat);
				}
			}else if(event.getSource()==entregado){
				Prestamo prestamoSel=(Prestamo) tvPrestamo.getSelectionModel().getSelectedItem();
				if(prestamoSel!=null){
					mPrestamo.actualizarPrestamo(prestamoSel.getIdPrestamo());
				}
			}else if(event.getSource()==eliminarC){
				Categoria catDel=(Categoria) tvCategoria.getSelectionModel().getSelectedItem();
				if(catDel!=null){
					mCategoria.eliminarCategoria(catDel.getIdCategoria());
				}

			}

		}else if(event instanceof KeyEvent){
			if(event.getSource()==buscarU){
				try{
					tvUsuario = new TableView<Usuario>(mUsuario.getListaUsuariosN(buscarU.getText()));			
					editar.addEventHandler(ActionEvent.ACTION,this);			
					eliminarU.addEventHandler(ActionEvent.ACTION,this);
					actualizarU.addEventHandler(ActionEvent.ACTION,this);
					agregarU.addEventHandler(ActionEvent.ACTION,this);
				    columaNombre.setCellValueFactory(new PropertyValueFactory<Usuario,String>("nombreUsuario"));
					columaNombre.setPrefWidth(100);				
				    columnaPass.setCellValueFactory(new PropertyValueFactory<Usuario,String>("contrasenia"));
					columnaPass.setPrefWidth(100);
					columnaRol = new TableColumn("ROL");  
				    columnaRol.setCellValueFactory(new PropertyValueFactory<Usuario,String>("nombreRol"));
					columnaRol.setPrefWidth(100);
					tvUsuario.getColumns().setAll(columaNombre,columnaPass,columnaRol);
					tvUsuario.setPrefWidth(302);
					gPanelPrincipal.add(tvUsuario,0,0,2,25);
					gPanelPrincipal.add(agregarU,3,1);
					gPanelPrincipal.add(editar,3,2);
					gPanelPrincipal.add(eliminarU,3,3);
					gPanelPrincipal.add(actualizarU,3,4);
					gPanelPrincipal.setVgap(8);
					gPanelPrincipal.setHgap(8);
					tvUsuario.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
					bpPrincipal.setLeft(gPanelPrincipal);
				}catch(IllegalArgumentException illeals){

				}
			}else if(event.getSource()==buscarE){
				try{
					mEditorial=new ManejadorEditorial(conexion);
					tvEditorial = new TableView<Editorial>(mEditorial.getListaEditorialN(buscarE.getText()));			
					editarE.addEventHandler(ActionEvent.ACTION,this);			
					eliminarE.addEventHandler(ActionEvent.ACTION,this);
					actualizarE.addEventHandler(ActionEvent.ACTION,this);
					agregarE.addEventHandler(ActionEvent.ACTION,this);
					columaNombreE = new TableColumn("EDITORIAL"); 
					columaNombreE.setCellValueFactory(new PropertyValueFactory<Editorial,String>("nombre"));
					columaNombreE.setPrefWidth(175);
					tvEditorial.getColumns().add(columaNombreE);				
					gPanelPrincipal.add(tvEditorial,0,0,2,25);
					gPanelPrincipal.add(agregarE,3,1);
					gPanelPrincipal.add(editarE,3,2);
					gPanelPrincipal.add(eliminarE,3,3);
					gPanelPrincipal.add(actualizarE,3,4);
					gPanelPrincipal.setVgap(8);
					gPanelPrincipal.setHgap(8);
					tvEditorial.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
					tvEditorial.setPrefWidth(178);
					bpPrincipal.setLeft(gPanelPrincipal);
				}catch(IllegalArgumentException illeals){

				}
			}else if(event.getSource()==buscarL){
				try{
					mLibro=new ManejadorLibro(conexion);			
					tvLibro = new TableView<Libro>(mLibro.getListaLibroN(buscarL.getText()));	
					buscarL.addEventHandler(KeyEvent.KEY_PRESSED,this);	
					agregarL.addEventHandler(ActionEvent.ACTION,this);
					eliminarL.addEventHandler(ActionEvent.ACTION,this);
					seeDetails.addEventHandler(ActionEvent.ACTION,this);
					columnaEditorial=new TableColumn("EDITORIAL");
					columnaEditorial.setCellValueFactory(new PropertyValueFactory<Libro,String>("nombreEditorial"));
					columnaEditorial.setPrefWidth(100);
					columnaNombreL=new TableColumn("LIBRO");	
					columnaNombreL.setCellValueFactory(new PropertyValueFactory<Libro,String>("nombreLibro"));
					columnaNombreL.setPrefWidth(100);	
					columnaCategoria=new TableColumn("CATEGORIA");
					columnaCategoria.setCellValueFactory(new PropertyValueFactory<Libro,String>("nombreCategoria"));
					columnaCategoria.setPrefWidth(75);		
					columnaEdicion=new TableColumn("EDICION");
					columnaEdicion.setCellValueFactory(new PropertyValueFactory<Libro,String>("edicion"));
					columnaEdicion.setPrefWidth(65);	
					columnaPaginas=new TableColumn("PAGINAS");
					columnaPaginas.setCellValueFactory(new PropertyValueFactory<Libro,String>("paginasLibro"));
					columnaPaginas.setPrefWidth(65);
					tvLibro.getColumns().addAll(columnaEditorial,columnaNombreL,columnaCategoria,columnaEdicion,columnaPaginas);	
					tvLibro.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
					tvLibro.setPrefWidth(407);
					gPanelPrincipal.getChildren().clear();
					gPanelPrincipal.add(tvLibro,0,0,2,25);
					gPanelPrincipal.add(agregarL,3,1);
					gPanelPrincipal.add(eliminarL,3,2);
					gPanelPrincipal.add(seeDetails,3,3);
					gPanelPrincipal.setVgap(8);
					gPanelPrincipal.setHgap(8);
					bpPrincipal.setLeft(gPanelPrincipal);
				}catch(IllegalArgumentException illeals){

				}
			}else if(event.getSource()==buscarA){
				try{
					mAutor=new ManejadorAutor(conexion);
					tvAutor=new TableView<Autor>(mAutor.getListaAutorN(buscarA.getText()));
					editarA.addEventHandler(ActionEvent.ACTION,this);			
					eliminarA.addEventHandler(ActionEvent.ACTION,this);
					actualizarA.addEventHandler(ActionEvent.ACTION,this);
					agregarA.addEventHandler(ActionEvent.ACTION,this);
					columnaNombreA=new TableColumn("AUTOR");
					columnaNombreA.setCellValueFactory(new PropertyValueFactory<Autor,String>("nombre"));
					columnaNombreA.setPrefWidth(100);
					columnaNombreLA=new TableColumn("LIBRO");
					columnaNombreLA.setCellValueFactory(new PropertyValueFactory<Autor,String>("nombreLibro"));
					columnaNombreLA.setPrefWidth(100);
					tvAutor.getColumns().addAll(columnaNombreA,columnaNombreLA);
					tvAutor.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
					tvAutor.setPrefWidth(202);
					gPanelPrincipal.add(tvAutor,0,0,2,25);
					gPanelPrincipal.add(agregarA,3,1);
					gPanelPrincipal.add(editarA,3,2);
					gPanelPrincipal.add(eliminarA,3,3);
					gPanelPrincipal.add(actualizarA,3,4);
					gPanelPrincipal.setVgap(8);
					gPanelPrincipal.setHgap(8);
					bpPrincipal.setLeft(gPanelPrincipal);
				}catch(IllegalArgumentException illeals){

				}
			}else if(event.getSource()==buscarC){
				try{
					mCategoria=new ManejadorCategoria(conexion);
					tvCategoria=new TableView<Categoria>(mCategoria.getListaCategoriaN(buscarC.getText()));
					buscarC.addEventHandler(KeyEvent.KEY_PRESSED,this);	
					editarC.addEventHandler(ActionEvent.ACTION,this);			
					eliminarC.addEventHandler(ActionEvent.ACTION,this);
					actualizarC.addEventHandler(ActionEvent.ACTION,this);
					agregarC.addEventHandler(ActionEvent.ACTION,this);
					columnaCategoriaN=new TableColumn("CATEGORIA");
					columnaCategoriaN.setCellValueFactory(new PropertyValueFactory<Categoria,String>("nombre"));
					columnaCategoriaN.setPrefWidth(100);
					tvCategoria.getColumns().addAll(columnaCategoriaN);
					tvCategoria.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
					tvCategoria.setPrefWidth(102);
					gPanelPrincipal.add(tvCategoria,0,0,2,25);
					gPanelPrincipal.add(agregarC,3,1);
					gPanelPrincipal.add(editarC,3,2);
					gPanelPrincipal.add(eliminarC,3,3);
					gPanelPrincipal.add(actualizarC,3,4);
					gPanelPrincipal.setVgap(8);
					gPanelPrincipal.setHgap(8);
					bpPrincipal.setLeft(gPanelPrincipal);
				}catch(IllegalArgumentException illeals){

				}
			}else if(event.getSource()==buscarS){
				try{
					editarSubC.addEventHandler(ActionEvent.ACTION,this);			
					eliminarSubC.addEventHandler(ActionEvent.ACTION,this);
					actualizarSubC.addEventHandler(ActionEvent.ACTION,this);
					agregarSubC.addEventHandler(ActionEvent.ACTION,this);
					mSubCategoria=new ManejadorSubCategoria(conexion);
					tvSubCategoria=new TableView<SubCategoria>(mSubCategoria.getListaSubCategoriaN(buscarS.getText()));
					columnaSubCategoriaN=new TableColumn("SUB-CATEGORIA");
					columnaSubCategoriaN.setCellValueFactory(new PropertyValueFactory<Prestamo,String>("nombre"));
					columnaSubCategoriaN.setPrefWidth(100);
					tvSubCategoria.setPrefWidth(102);
					tvSubCategoria.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
					tvSubCategoria.getColumns().addAll(columnaSubCategoriaN);
					gPanelPrincipal.add(tvSubCategoria,0,0,2,25);
					gPanelPrincipal.add(agregarSubC,3,1);
					gPanelPrincipal.add(editarSubC,3,2);
					gPanelPrincipal.add(eliminarSubC,3,3);
					gPanelPrincipal.add(actualizarSubC,3,4);			
					gPanelPrincipal.setVgap(8);
					gPanelPrincipal.setHgap(8);
					bpPrincipal.setLeft(gPanelPrincipal);
				}catch(IllegalArgumentException illeals){

				}
			}
		}
	}
	public Button getEliminarU(){
		return eliminarU;
	}
	public Button getEditar(){
		return editar;
	}
	public Button getActualizarU(){
		return actualizarU;
	}
	public Button getAceptarEdit(){
		return aceptarEdit;
	}
	public Button getCancel(){
		return cancel;
	}
	public Button getAgregarU(){
		return agregarU;
	}
	public Button getAgregarUs(){
		return agregarUs;
	}
	public Button getAceptarE(){
		return aceptarE;
	}
	public Button getEditarE(){
		return editarE;
	}
	public Button getActualizarE(){
		return actualizarE;
	}
	public Button getEliminarE(){
		return eliminarE;
	}
	public Button getAgregarL(){
		return agregarL;
	}
	public Button getEliminarL(){
		return eliminarL;
	}
	public Button getAceptarL(){
		return aceptarL;
	}
	public Button getSeeDetails(){
		return seeDetails;
	}
	public Button getBuscarCat(){
		return buscarCat;
	}
	public Button getAgregarE(){
		return agregarE;
	}
	public Button getAgregarEn(){
		return agregarEn;
	}
	public Button getAgregarA(){
		return agregarA;
	}
	public Button getEliminarA(){
		return eliminarA;
	}
	public Button getEditarA(){
		return editarA;
	}
	public Button getActualizarA(){
		return actualizarA;
	}
	public Button getBuscarLA(){
		return buscarLA;
	}
	public Button getAceptarLA(){
		return aceptarLA;
	}
	public Button getAceptarA(){
		return aceptarA;
	}
	public Button getAceptarEditA(){
		return aceptarEditA;
	}
	public Button getAgregarC(){
		return agregarC;
	}
	public Button getAceptarC(){
		return aceptarC;
	}
	public Button getEdicarC(){
		return editarC;
	}
	public Button getEliminarC(){
		return eliminarC;
	}
	public Button getActualizarC(){
		return actualizarC;
	}
	public Button getAceptarEditarC(){
		return aceptarEditarC;
	}
	public Button getAgregarSubC(){
		return agregarSubC;
	}
	public Button getAceptarSubC(){
		return aceptarSubC;
	}
	public Button getEdicarSubC(){
		return editarSubC;
	}
	public Button getEliminarSubC(){
		return eliminarSubC;
	}
	public Button getActualizarSubC(){
		return actualizarSubC;
	}
	public Button getAceptarEditarSubC(){
		return aceptarEditarSubC;
	}
	public Button getAgregarAL(){
		return agregarAL;
	}
	public Button getBtnEditarSub(){
		return btnEditarSub;
	}
	public Button getBuscarBook(){
		return buscarBook;
	}
	public Label getLblContra2(){
		return lblContra2;
	}
	public Label getLblContra(){
		return lblContra;
	}
	public Label getLblErr(){
		return lblErr;
	}
	public Label getTblNombreL(){
		return lblNombreL;
	}
	public Label getLblPaginas(){
		return lblPaginas;
	}
	public Label getLblEdicion(){
		return lblEdicion;
	}
	public Label getLblCategoria(){
		return lblCategoria;
	}
	public Label getLblNombreA(){
		return lblNombreA;
	}
	public Label getLblLibroA(){
		return lblLibroA;
	}
	public Label getLblNombreSC(){
		return lblNombreSC;
	}
}
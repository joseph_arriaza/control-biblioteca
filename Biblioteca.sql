CREATE DATABASE Biblioteca
GO
USE Biblioteca
GO 
CREATE TABLE Rol (
idRol INT IDENTITY (1,1) NOT NULL,
nombreRol VARCHAR (50) NOT NULL,
PRIMARY KEY (idRol)
);
CREATE TABLE Usuario(
	idUsuario INT IDENTITY(1,1) NOT NULL,
	nombre VARCHAR(255) NOT NULL,
	contrasenia VARCHAR(255) NOT NULL,
	idRol INT NOT NULL,
	PRIMARY KEY(idUsuario),
	FOREIGN KEY(idRol) REFERENCES Rol(idRol)
);
CREATE TABLE Categoria(
	idCategoria INT IDENTITY (1,1) NOT NULL,
	categoria VARCHAR (50) NOT NULL,
	PRIMARY KEY (idCategoria)
);
CREATE TABLE Editorial(
	idEditorial INT IDENTITY (1,1) NOT NULL,
	editorial VARCHAR (255) NOT NULL,
	PRIMARY KEY (idEditorial)
);
CREATE TABLE Libro(
	idLibro INT IDENTITY (1,1) NOT NULL,
	idCategoria INT NOT NULL,
	idEditorial INT NOT NULL,
	nombreLibro VARCHAR (255) NOT NULL,
	edicion INT NOT NULL,
	paginasLibro INT NULL,
	PRIMARY KEY (idLibro),
	FOREIGN KEY (idCategoria) REFERENCES Categoria(idCategoria),
	FOREIGN KEY (idEditorial) REFERENCES Editorial(idEditorial)
);
CREATE TABLE Autor(
	idAutor INT IDENTITY (1,1) NOT NULL,
	idLibro INT NOT NULL,
	nombreAutor VARCHAR (255) NOT NULL,
	PRIMARY KEY (idAutor),
	FOREIGN KEY (idLibro) REFERENCES Libro(idLibro)
);
CREATE TABLE SubCategoria(
	idSubCategoria INT IDENTITY(1,1) PRIMARY KEY,
	nombre VARCHAR(50) NOT NULL
);
CREATE TABLE VarSubCategoria(
	idSubcategoria INT NOT NULL,
	idLibro INT NOT NULL,
	PRIMARY KEY(idSubCategoria,idLibro),
	FOREIGN KEY(idSubCategoria) REFERENCES SubCategoria(idSubCategoria),
	FOREIGN KEY(idLibro) REFERENCES Libro(idLibro)
);
CREATE TABLE Prestamo(
	idPrestamo INT IDENTITY (1,1) PRIMARY KEY,
	idUsuario INT NOT NULL,
	idLibro INT NOT NULL,
	fecha VARCHAR(255) NOT NULL,
	estado VARCHAR(255) NOT NULL,
	costo INT NOT NULL,
	fechaEntrega VARCHAR(255),
	FOREIGN KEY(idUsuario) REFERENCES Usuario(idUsuario),
	FOREIGN KEY(idLibro) REFERENCES Libro(idLibro)
);
GO
--NUEVA CATEgoria
CREATE PROCEDURE nuevoCategoria @idCategoria INT, @Categoria VARCHAR (50)
AS
BEGIN 
INSERT INTO Categoria(idCategoria, categoria)VALUES (@idCategoria, @Categoria);
END
GO
--modificar categoria
CREATE PROCEDURE modificarCategoria @idCategoria INT, @categoria VARCHAR (50)
AS 
BEGIN
UPDATE Categoria SET categoria = @categoria     WHERE idCategoria = @idCategoria;
END
GO
--nueva editorial
CREATE PROCEDURE nuevoEditorial @idCategoria INT, @editorial VARCHAR (255)
AS
BEGIN 
INSERT INTO Categoria(idCategoria, categoria) VALUES (@idCategoria, @editorial);
END
GO
--modificar editorial
CREATE PROCEDURE modificarEditorial @idEditorial INT, @editorial VARCHAR (255)
AS 
BEGIN
UPDATE Editorial SET editorial = @editorial  WHERE idEditorial = @idEditorial;
END
GO
--crear usuario
CREATE PROCEDURE nuevoUsuario @idRol INT,@nombreUsuario VARCHAR (50),@contrasenia VARCHAR (50)
AS
BEGIN 
INSERT INTO Usuario (idRol, nombre, contrasenia)VALUES (@idRol, @nombreUsuario, @contrasenia);
END
GO
--modificarUsuario
GO
CREATE PROCEDURE modificarUsuario @idUsuario INT,@idRol INT,@nombreUsuario VARCHAR (255),@contrasenia VARCHAR (255)
AS 
BEGIN
UPDATE Usuario SET nombre = @nombreUsuario, idRol = @idRol, contrasenia = @contrasenia WHERE idUsuario = @idUsuario;
END
GO 
--consulta usuario
CREATE VIEW consultarUsuario
AS 
	SELECT Usuario.idUsuario,Rol.nombreRol,
	Usuario.nombre,Usuario.contrasenia
	FROM Usuario INNER JOIN Rol
	ON Usuario.idRol = Rol.idRol;
GO
--insertar rol
CREATE PROCEDURE insertar_Rol @nombreRol VARCHAR (255)
AS
BEGIN
INSERT INTO Rol (nombreRol) VALUES (@nombreRol)
END
GO
--llenar usuario
CREATE PROCEDURE listaDeUsuarios
AS
BEGIN
	SELECT Usuario.idUsuario,Usuario.nombre, Usuario.contrasenia, Usuario.idRol, Rol.nombreRol AS nombreRol FROM Usuario
		INNER JOIN Rol ON Usuario.idRol=Rol.idRol
END
GO
--autenticar usuario
CREATE PROCEDURE autenticar @nombre VARCHAR(255), @pass VARCHAR(255)
AS
BEGIN
	SELECT idUsuario,Usuario.idRol,Usuario.nombre,contrasenia,Rol.nombreRol AS nombreRol,contrasenia FROM Usuario
		INNER JOIN Rol ON Usuario.idRol=Rol.idRol
		WHERE nombre=@nombre AND contrasenia=@pass
END
GO
exec autenticar 'admin','' SELECT * FROM Usuario WHERE 'a'='a'
GO
--agregar usuario
CREATE PROCEDURE addUser @nombre VARCHAR(255),@contrasenia VARCHAR(255), @idRol INT
AS
BEGIN
	INSERT INTO Usuario(nombre,contrasenia,idRol)VALUES(@nombre,@contrasenia,@idRol)
END
GO
---ver lista editorial
CREATE PROCEDURE listaDeEditorial
AS
BEGIN
	SELECT idEditorial,editorial FROM Editorial
END
GO
--eliminar usuario
CREATE PROCEDURE eliminarUsuario @idUsuario INT
AS
BEGIN
	DELETE FROM Usuario WHERE idUsuario=@idUsuario
END
GO
--lista de rol
CREATE PROCEDURE listaRol
AS
BEGIN
	SELECT idRol, nombreRol FROM Rol
END
GO
--agregar editorial
CREATE PROCEDURE agregarEditorial @editorial VARCHAR(255)
AS
BEGIN
	INSERT INTO Editorial(editorial) VALUES (@editorial)
END
GO
--editar editorial
CREATE PROCEDURE editarEditorial @nombre VARCHAR(255), @id INT
AS
BEGIN
	UPDATE Editorial SET editorial=@nombre WHERE idEditorial=@id
END
GO
--eliminar editorial
CREATE PROCEDURE eliminarEditorial @id INT
AS
BEGIN
	DECLARE @idLibro INT
	DECLARE cEliminarAutor CURSOR
	FOR
		SELECT idLibro FROM Libro WHERE idEditorial=@id
	OPEN cEliminarAutor
	FETCH cEliminarAutor INTO @idLibro
	WHILE(@@FETCH_STATUS=0)
	BEGIN
		PRINT @idLibro
		DELETE FROM Autor WHERE idLibro=@idLibro	
		FETCH cEliminarAutor INTO @idLibro	
	END
	CLOSE cEliminarAutor
	DEALLOCATE cEliminarAutor
	DELETE FROM Libro WHERE idEditorial=@id
	DELETE FROM Editorial WHERE idEditorial=@id
END
GO
--LISTA LIBROS
CREATE PROCEDURE listaDeLibro
AS
BEGIN
	SELECT Libro.idLibro AS idLibro, Libro.idCategoria AS idCategoria, Libro.idEditorial AS idEditorial, Libro.nombreLibro AS nombreLibro,
	Libro.edicion AS edicion, Libro.paginasLibro AS paginasLibro, Editorial.editorial AS nombreEditorial, Categoria.categoria AS nombreCategoria
	FROM Libro
		INNER JOIN Editorial ON Libro.idEditorial=Editorial.idEditorial
		INNER JOIN Categoria ON Libro.idCategoria=Categoria.idCategoria
END
GO
--ELIMINAR LIBRO
CREATE PROCEDURE eliminarLibro @idLibro INT
AS
BEGIN
	DELETE FROM Autor WHERE idLibro=@idLibro
	DELETE FROM Libro WHERE idLibro=@idLibro
END
GO
--AGREGAR LIBRO
CREATE PROCEDURE agregarLibro @idCategoria INT, @idEditorial INT, @nombreLibro VARCHAR(255), @edicion INT, @paginasLibro INT
AS
BEGIN
	INSERT INTO Libro(idCategoria,idEditorial,nombreLibro,edicion,paginasLibro)
		VALUES(@idCategoria,@idEditorial,@nombreLibro,@edicion,@paginasLibro)
END
GO
--LISTA CATEGORIA
CREATE PROCEDURE listaCategoria
AS
BEGIN
	SELECT idCategoria,categoria FROM Categoria
END
GO
--AGREGAR CATEGORIA
CREATE PROCEDURE agregarCategoria @nombre VARCHAR(255)
AS
BEGIN
	INSERT INTO Categoria(categoria) VALUES(@nombre)
END
GO
--ELIMINAR CATEGORIA
CREATE PROCEDURE eliminarCategoria @idCat INT
AS
BEGIN
	DECLARE @idLibro INT
	DECLARE cEliminarAutor CURSOR
	FOR
		SELECT idLibro FROM Libro WHERE idCategoria=@idCat
	OPEN cEliminarAutor
	FETCH cEliminarAutor INTO @idLibro
	WHILE(@@FETCH_STATUS=0)
	BEGIN
		DELETE FROM Autor WHERE idLibro=@idLibro	
		FETCH cEliminarAutor INTO @idLibro	
	END
	CLOSE cEliminarAutor
	DEALLOCATE cEliminarAutor
	DELETE FROM Libro WHERE idCategoria=@idCat
	DELETE FROM Categoria WHERE idCategoria=@idCat
END
GO
--ACTUALIZAR CATEGORIA
CREATE PROCEDURE actualizarCategoria @idCat INT,@nombre VARCHAR(255)
as
BEGIN
	UPDATE Categoria SET categoria=@nombre WHERE idCategoria=@idCat
END
GO
--lista autor
CREATE PROCEDURE listaAutor
AS
BEGIN
	SELECT Autor.idAutor AS idAutor,Autor.nombreAutor AS nombre, Autor.idLibro AS idLibro, Libro.nombreLibro AS nombreLibro FROM Autor
		INNER JOIN Libro ON Autor.idLibro=Libro.idLibro
END
GO
--actualizara autor
CREATE PROCEDURE actualizarAutor @id INT, @nombreAutor VARCHAR(255)
AS
BEGIN
	UPDATE Autor SET nombreAutor=@nombreAutor WHERE idAutor=@id
END
GO
--ELIMINAR AUTOR
CREATE PROCEDURE eliminarAutor @idAutor INT
AS
BEGIN
	DELETE FROM Autor WHERE idAutor=@idAutor
END
GO
--AGREGAR AUTOR
CREATE PROCEDURE agregarAutor @nombreAutor VARCHAR(255), @idLibro INT
AS
BEGIN
	INSERT INTO Autor(nombreAutor,idLibro) VALUES (@nombreAutor,@idLibro)
END
GO
--lista de prestamos
CREATE PROCEDURE listaPrestamo
AS
BEGIN
	SELECT Prestamo.fechaEntrega AS fechaEntrega,Prestamo.estado AS estado, Prestamo.idPrestamo AS idPrestamo,Prestamo.idUsuario AS idUsuario,Prestamo.idLibro AS idLibro,Prestamo.fecha AS fecha,Prestamo.estado AS estado,Prestamo.costo AS costo, Libro.nombreLibro AS nombreLibro, Usuario.nombre AS nombre FROM Prestamo
		 INNER JOIN Libro ON Prestamo.idLibro=Libro.idLibro
		 INNER JOIN Usuario ON Prestamo.idUsuario=Usuario.idUsuario
END
GO
--actualizar mi lista de prestamo
CREATE PROCEDURE actualizarMiLista @idUsuario INT
AS
BEGIN
	SELECT Prestamo.fechaEntrega AS fechaEntrega,Prestamo.estado AS estado, Prestamo.idPrestamo AS idPrestamo,Prestamo.idUsuario AS idUsuario,Prestamo.idLibro AS idLibro,Prestamo.fecha AS fecha,Prestamo.estado AS estado,Prestamo.costo AS costo, Libro.nombreLibro AS nombreLibro, Usuario.nombre AS nombre FROM Prestamo
		INNER JOIN Libro ON Prestamo.idLibro=Libro.idLibro
		INNER JOIN Usuario ON Prestamo.idUsuario=Usuario.idUsuario
		WHERE Prestamo.idUsuario=@idUsuario
END
GO
--agregar prestamo
CREATE PROCEDURE agregarPrestamo @idUusario INT,@idLibro INT,@fecha VARCHAR(255)
AS
BEGIN
	INSERT INTO Prestamo(idUsuario,idLibro,fecha,estado,costo)
		VALUES(@idUusario,@idLibro,@fecha,'pendiente',25)
END
GO
--actualizar prestamo
CREATE PROCEDURE actualizarPrestamo @idPrestamo INT,@fechaEntrega VARCHAR(255)
AS
BEGIN
	UPDATE Prestamo SET estado='entregado',fechaEntrega=@fechaEntrega WHERE idPrestamo=@idPrestamo
END
GO
--listaSubCategoria
CREATE PROCEDURE listaSubCategoria
AS
BEGIN
	SELECT idSubCategoria,nombre FROM SubCategoria
END
GO
--actualizarSubCategoria
CREATE PROCEDURE actualizarSubCategoria @id INT,@nombre VARCHAR(255)
AS
BEGIN
	UPDATE SubCategoria SET nombre=@nombre WHERE idSubCategoria=@id
END
GO
--eliminarSubCategoria
CREATE PROCEDURE eliminarSubCategoria @id INT
AS
BEGIN
	DELETE FROM VarSubCategoria WHERE idSubcategoria=@id
	DELETE FROM SubCategoria WHERE idSubcategoria=@id
END
GO
--agregarSubCategoria
CREATE PROCEDURE agregarSubCategoria @nombre VARCHAR(255)
AS
BEGIN
	INSERT INTO SubCategoria(nombre)VALUES(@nombre)
END
GO
--listaVarSubCategoria
CREATE PROCEDURE listaVarSubCategoria
AS
BEGIN
	SELECT VarSubCategoria.idSubcategoria AS idSubCategoria, VarSubCategoria.idLibro AS idLibro, SubCategoria.nombre AS nombreCat, Libro.nombreLibro AS nombreLibro FROM VarSubCategoria
		INNER JOIN SubCategoria ON VarSubCategoria.idSubCategoria=SubCategoria.idSubCategoria
		INNER JOIN Libro ON VarSubCategoria.idLibro=Libro.idLibro
END
GO
--listaVarSubCategoriaL
CREATE PROCEDURE listaVarSubCategoriaL @id INT
AS
BEGIN
	SELECT VarSubCategoria.idSubcategoria AS idSubCategoria, VarSubCategoria.idLibro AS idLibro, SubCategoria.nombre AS nombreCat, Libro.nombreLibro AS nombreLibro FROM VarSubCategoria
		INNER JOIN SubCategoria ON VarSubCategoria.idSubCategoria=SubCategoria.idSubCategoria
		INNER JOIN Libro ON VarSubCategoria.idLibro=Libro.idLibro
		WHERE VarSubCategoria.idLibro=@id
END
GO
--agregarVarSubCategoria
CREATE PROCEDURE agregarVarSubCategoria @idLibro INT,@idSubCat INT
AS
BEGIN
	INSERT INTO VarSubCategoria(idLibro,idSubcategoria) VALUES(@idLibro,@idSubCat)
END
GO
--ACTUALIZAR LISTA valor
CREATE PROCEDURE listaDeLibroValorBuscar @nombre VARCHAR(500)
AS
BEGIN
	SELECT Libro.idLibro AS idLibro, Libro.idCategoria AS idCategoria, Libro.idEditorial AS idEditorial, Libro.nombreLibro AS nombreLibro,
	Libro.edicion AS edicion, Libro.paginasLibro AS paginasLibro, Editorial.editorial AS nombreEditorial, Categoria.categoria AS nombreCategoria
	FROM Libro
		INNER JOIN Editorial ON Libro.idEditorial=Editorial.idEditorial
		INNER JOIN Categoria ON Libro.idCategoria=Categoria.idCategoria
		WHERE Libro.nombreLibro LIKE '%'+@nombre+'%'
END
GO
--generar reporte
CREATE PROCEDURE Reporte
AS
BEGIN
	SELECT TOP 10 idLibro,COUNT(*) AS idUsuario FROM Prestamo
		GROUP BY idLibro
END
GO
--generar reporte
CREATE PROCEDURE generarReporte @idLibro INT
AS
BEGIN
	SELECT Libro.idLibro AS idLibro, Libro.idCategoria AS idCategoria, Libro.idEditorial AS idEditorial, Libro.nombreLibro AS nombreLibro,
	Libro.edicion AS edicion, Libro.paginasLibro AS paginasLibro, Editorial.editorial AS nombreEditorial, Categoria.categoria AS nombreCategoria
	FROM Libro
		INNER JOIN Editorial ON Libro.idEditorial=Editorial.idEditorial
		INNER JOIN Categoria ON Libro.idCategoria=Categoria.idCategoria
		WHERE idLibro=@idLibro
END
GO
/*use dbbodegaK
drop database biblioteca*/
--LISTA DE USUARIOS N
CREATE PROCEDURE listaDeUsuariosN @nombre VARCHAR
AS
BEGIN
	SELECT Usuario.idUsuario,Usuario.nombre, Usuario.contrasenia, Usuario.idRol, Rol.nombreRol AS nombreRol FROM Usuario
		INNER JOIN Rol ON Usuario.idRol=Rol.idRol
		WHERE Usuario.nombre LIKE '%'+@nombre+'%'
END
GO
--LISTA DE EDITORIAL N
CREATE PROCEDURE listaDeEditorialN @nombre VARCHAR(255)
AS
BEGIN
		SELECT idEditorial,editorial FROM Editorial
		WHERE editorial LIKE '%'+@nombre+'%'
END
GO
--listaDeLibroN
CREATE PROCEDURE listaDeLibroN @nombre VARCHAR(255)
AS
BEGIN
	SELECT Libro.idLibro AS idLibro, Libro.idCategoria AS idCategoria, Libro.idEditorial AS idEditorial, Libro.nombreLibro AS nombreLibro,
	Libro.edicion AS edicion, Libro.paginasLibro AS paginasLibro, Editorial.editorial AS nombreEditorial, Categoria.categoria AS nombreCategoria
	FROM Libro
		INNER JOIN Editorial ON Libro.idEditorial=Editorial.idEditorial
		INNER JOIN Categoria ON Libro.idCategoria=Categoria.idCategoria
		WHERE Libro.nombreLibro LIKE  '%'+@nombre+'%'
END
GO
--listaAutorN
CREATE PROCEDURE listaAutorN @nombre VARCHAR(255)
AS
BEGIN
	SELECT Autor.idAutor AS idAutor,Autor.nombreAutor AS nombre, Autor.idLibro AS idLibro, Libro.nombreLibro AS nombreLibro FROM Autor
		INNER JOIN Libro ON Autor.idLibro=Libro.idLibro
		WHERE Autor.nombreAutor LIKE  '%'+@nombre+'%'
END
GO
--LITA CATEGORIA N
CREATE PROCEDURE listaCategoriaN @nombre VARCHAR(255)
AS
BEGIN
	SELECT idCategoria,categoria FROM Categoria
		WHERE categoria LIKE  '%'+@nombre+'%'
END
GO
--listaSubCategoriaN
CREATE PROCEDURE listaSubCategoriaN @nombre VARCHAR(255)
AS
BEGIN
	SELECT idSubCategoria,nombre FROM SubCategoria
	WHERE nombre LIKE  '%'+@nombre+'%'
END
GO
INSERT INTO Rol (nombreRol) VALUES ('administrador')
INSERT INTO Rol (nombreRol) VALUES ('editor')
INSERT INTO Rol (nombreRol) VALUES ('visualizador de reporte')
INSERT INTO Rol (nombreRol) VALUES ('alumno')
INSERT INTO Usuario(nombre,contrasenia,idRol) VALUES ('admin','admin',1)
INSERT INTO Usuario(nombre,contrasenia,idRol) VALUES ('editor','editor',2)
INSERT INTO Usuario(nombre,contrasenia,idRol) VALUES ('visual','visual',3)
INSERT INTO Usuario(nombre,contrasenia,idRol) VALUES ('alumno','alumno',4)

INSERT INTO Categoria(categoria) VALUES ('Fantasia');
INSERT INTO Categoria(categoria) VALUES ('Aventura');
INSERT INTO Categoria(categoria) VALUES ('Historia');

INSERT INTO Editorial(editorial) VALUES ('Casa Grande');
INSERT INTO Editorial(editorial) VALUES ('Santilana');
INSERT INTO Editorial(editorial) VALUES ('San Pedrito Ayanpucc Cuc');

INSERT INTO Libro(idCategoria, idEditorial, nombreLibro, edicion, paginasLibro)
	VALUES (1, 1, 'Harry Potter', 2, 586);
INSERT INTO Libro(idCategoria, idEditorial, nombreLibro, edicion, paginasLibro)
	VALUES (1, 1, 'El se�or de los tornillos', 1, 1430);
INSERT INTO Libro(idCategoria, idEditorial, nombreLibro, edicion, paginasLibro)
	VALUES (2, 2, 'El diario de Ana Frank', 1, 487);
INSERT INTO Libro( idCategoria, idEditorial, nombreLibro, edicion, paginasLibro)
	VALUES (3, 3, 'Estudios Sociales', 5, 234);

INSERT INTO Autor(idLibro, nombreAutor) VALUES (1, 'J. K. Rowling.');
INSERT INTO Autor(idLibro, nombreAutor) VALUES (2, 'J.R.R. Tolkein');
INSERT INTO Autor(idLibro, nombreAutor) VALUES (3, 'Anne Frank');
INSERT INTO Autor(idLibro, nombreAutor) VALUES (4, 'Franz Kafka');
INSERT INTO Prestamo(idUsuario,idLibro,fecha,estado,costo)VALUES(4,1,'01/06/2014','pendiente',25)
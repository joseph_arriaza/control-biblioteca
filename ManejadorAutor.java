package org.carloscarrera.manejadores;

import java.sql.SQLException;
import java.sql.ResultSet;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import javafx.beans.property.SimpleListProperty;

import org.carloscarrera.conexion.Conexion;
import org.carloscarrera.bean.Autor;
public class ManejadorAutor{
	private Conexion conexion;
	private ObservableList<Autor> listaDeAutors;
	private ObservableList<Autor> listaDeAutorsN;
	public ManejadorAutor(Conexion conexion){
		this.listaDeAutors = FXCollections.observableArrayList();
		this.listaDeAutorsN = FXCollections.observableArrayList();
		this.conexion = conexion;
		actualizarLista();
	}
	public void actualizarLista(){
		ResultSet resultado = conexion.getInstance().consultar("EXEC listaAutor");
		if(resultado!=null){
			listaDeAutors.clear();
			try{
				while(resultado.next()){
					Autor autor=new Autor(resultado.getInt("idAutor"),resultado.getInt("idLibro"),resultado.getString("nombre"),resultado.getString("nombreLibro"));
					listaDeAutors.add(autor);
				}
			}catch(SQLException sql){
				sql.printStackTrace();
			}
		}		
	}
	public void actualizarListaN(String valor){
		ResultSet resultado = conexion.getInstance().consultar("EXEC listaAutorN '"+valor+"'");
		if(resultado!=null){
			listaDeAutorsN.clear();
			try{
				while(resultado.next()){
					Autor autor=new Autor(resultado.getInt("idAutor"),resultado.getInt("idLibro"),resultado.getString("nombre"),resultado.getString("nombreLibro"));
					listaDeAutorsN.add(autor);
				}
			}catch(SQLException sql){
				sql.printStackTrace();
			}
		}		
	}
	public ObservableList<Autor> getListaAutor(){
		actualizarLista();
		return listaDeAutors;
	}
	public ObservableList<Autor> getListaAutorN(String valor){
		actualizarListaN(valor);
		return listaDeAutorsN;
	}
	public void actualizarAutor(int id,String valor){
		conexion.getInstance().ejecutar("EXEC actualizarAutor "+id+",'"+valor+"'");
		actualizarLista();
	}
	public void eliminarAutor(int id){
		conexion.getInstance().ejecutar("EXEC eliminarAutor "+id+"");
		actualizarLista();
	}
	public void agregarAutor(String nombre,int idLibro){
		conexion.getInstance().ejecutar("EXEC agregarAutor '"+nombre+"',"+idLibro+"");
		actualizarLista();
	}
}

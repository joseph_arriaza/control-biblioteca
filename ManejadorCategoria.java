package org.carloscarrera.manejadores;

import java.sql.SQLException;
import java.sql.ResultSet;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import javafx.beans.property.SimpleListProperty;

import org.carloscarrera.conexion.Conexion;
import org.carloscarrera.bean.Categoria;
public class ManejadorCategoria{
	private Conexion conexion;
	private ObservableList<Categoria> listaDeCategorias;
	private ObservableList<Categoria> listaDeCategoriasN;
	public ManejadorCategoria(Conexion conexion){
		this.listaDeCategorias = FXCollections.observableArrayList();
		this.listaDeCategoriasN = FXCollections.observableArrayList();
		this.conexion = conexion;
		actualizarLista();
	}
	public void actualizarLista(){
		ResultSet resultado = conexion.getInstance().consultar("EXEC listaCategoria");
		if(resultado!=null){
			listaDeCategorias.clear();
			try{
				while(resultado.next()){
					Categoria categoria=new Categoria(resultado.getInt("idCategoria"),resultado.getString("categoria"));
					listaDeCategorias.add(categoria);
				}
			}catch(SQLException sql){
				sql.printStackTrace();
			}
		}		
	}
	public void actualizarListaN(String valor){
		ResultSet resultado = conexion.getInstance().consultar("EXEC listaCategoriaN '"+valor+"'");
		if(resultado!=null){
			listaDeCategoriasN.clear();
			try{
				while(resultado.next()){
					Categoria categoria=new Categoria(resultado.getInt("idCategoria"),resultado.getString("categoria"));
					listaDeCategoriasN.add(categoria);
				}
			}catch(SQLException sql){
				sql.printStackTrace();
			}
		}		
	}
	public ObservableList<Categoria> getListaCategoria(){
		actualizarLista();
		return listaDeCategorias;
	}
	public ObservableList<Categoria> getListaCategoriaN(String valor){
		actualizarListaN(valor);
		return listaDeCategoriasN;
	}
	public void actualizarCategoria(int id,String valor){
		conexion.getInstance().ejecutar("EXEC actualizarCategoria "+id+",'"+valor+"'");
		actualizarLista();
	}
	public void eliminarCategoria(int id){
		conexion.getInstance().ejecutar("EXEC eliminarCategoria "+id+"");
		actualizarLista();
	}
	public void agregarCategoria(String nombre){
		conexion.getInstance().ejecutar("EXEC agregarCategoria '"+nombre+"'");
		actualizarLista();
	}
}

package org.carloscarrera.manejadores;

import java.sql.SQLException;
import java.sql.ResultSet;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import javafx.beans.property.SimpleListProperty;

import org.carloscarrera.conexion.Conexion;
import org.carloscarrera.bean.Editorial;
public class ManejadorEditorial{
	private Conexion conexion;
	private ObservableList<Editorial> listaDeEditorials;
	private ObservableList<Editorial> listaDeEditorialsN;
	public ManejadorEditorial(Conexion conexion){
		this.listaDeEditorials = FXCollections.observableArrayList();
		this.listaDeEditorialsN= FXCollections.observableArrayList();
		this.conexion = conexion;
		actualizarLista();
	}
	public void actualizarLista(){
		ResultSet resultado = conexion.getInstance().consultar("EXEC listaDeEditorial");
		if(resultado!=null){
			listaDeEditorials.clear();
			try{
				while(resultado.next()){
					Editorial editorial=new Editorial();
					editorial.setIdEditorial(resultado.getInt("idEditorial"));				
					editorial.setNombre(resultado.getString("editorial"));
					listaDeEditorials.add(editorial);
				}
			}catch(SQLException sql){
				sql.printStackTrace();
			}
		}		
	}
	public void actualizarListaN(String valor){
		ResultSet resultado = conexion.getInstance().consultar("EXEC listaDeEditorialN '"+valor+"'");
		if(resultado!=null){
			listaDeEditorialsN.clear();
			try{
				while(resultado.next()){
					Editorial editorial=new Editorial();
					editorial.setIdEditorial(resultado.getInt("idEditorial"));				
					editorial.setNombre(resultado.getString("editorial"));
					listaDeEditorialsN.add(editorial);
				}
			}catch(SQLException sql){
				sql.printStackTrace();
			}
		}		
	}
	public ObservableList<Editorial> getListaEditorial(){
		actualizarLista();
		return listaDeEditorials;
	}
	public ObservableList<Editorial> getListaEditorialN(String valor){
		actualizarListaN(valor);
		return listaDeEditorialsN;
	}
	public void eliminarEditorial(int id){
		conexion.getInstance().ejecutar("EXEC eliminarEditorial "+id+"");
		actualizarLista();
	}
	public void agregarEditorial(String nombre){
		conexion.getInstance().ejecutar("EXEC agregarEditorial '"+nombre+"'");
		actualizarLista();
	}
	public void actualizarEditorial(String valor,int idEditorial){
		conexion.getInstance().ejecutar("EXEC editarEditorial '"+valor+"',"+idEditorial);
		actualizarLista();
	}	
}

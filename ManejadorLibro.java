package org.carloscarrera.manejadores;

import java.sql.SQLException;
import java.sql.ResultSet;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import javafx.beans.property.SimpleListProperty;
import java.util.ArrayList;
import org.carloscarrera.conexion.Conexion;
import org.carloscarrera.manejadores.ManejadorPrestamo;
import org.carloscarrera.bean.Libro;
import org.carloscarrera.bean.Prestamo;
public class ManejadorLibro{
	private Conexion conexion;
	private ObservableList<Libro> listaDeLibros;
	private ObservableList<Libro> listaDeLibrosValor;
	private ObservableList<Libro> listaMasPretados;
	private ObservableList<Libro> listaDeLibrosN;
	private boolean primeraVez;
	public ManejadorLibro(Conexion conexion){
		this.listaDeLibros = FXCollections.observableArrayList();
		this.listaDeLibrosValor=FXCollections.observableArrayList();
		listaMasPretados=FXCollections.observableArrayList();
		listaDeLibrosN=FXCollections.observableArrayList();
		this.conexion = conexion;
		actualizarLista();
	}
	public void actualizarListaValor(String valor){
		ResultSet resultado = conexion.getInstance().consultar("EXEC listaDeLibroValorBuscar '"+valor+"'");
		if(resultado!=null){
			listaDeLibrosValor.clear();
			try{
				while(resultado.next()){
					Libro libro=new Libro();
					libro.setIdLibro(resultado.getInt("idLibro"));				
					libro.setIdCategoria(resultado.getInt("idCategoria"));
					libro.setIdEditorial(resultado.getInt("idEditorial"));
					libro.setNombreLibro(resultado.getString("nombreLibro"));
					libro.setEdicion(resultado.getInt("edicion"));
					libro.setPaginasLibro(resultado.getInt("paginasLibro"));
					libro.setNombreEditorial(resultado.getString("nombreEditorial"));
					libro.setNombreCategoria(resultado.getString("nombreCategoria"));
					listaDeLibrosValor.add(libro);
				}
			}catch(SQLException sql){
				sql.printStackTrace();
			}
		}		
	}
	public void actualizarLista(){
		ResultSet resultado = conexion.getInstance().consultar("EXEC listaDeLibro");
		if(resultado!=null){
			listaDeLibros.clear();
			try{
				while(resultado.next()){
					Libro libro=new Libro();
					libro.setIdLibro(resultado.getInt("idLibro"));				
					libro.setIdCategoria(resultado.getInt("idCategoria"));
					libro.setIdEditorial(resultado.getInt("idEditorial"));
					libro.setNombreLibro(resultado.getString("nombreLibro"));
					libro.setEdicion(resultado.getInt("edicion"));
					libro.setPaginasLibro(resultado.getInt("paginasLibro"));
					libro.setNombreEditorial(resultado.getString("nombreEditorial"));
					libro.setNombreCategoria(resultado.getString("nombreCategoria"));
					listaDeLibros.add(libro);
				}
			}catch(SQLException sql){
				sql.printStackTrace();
			}
		}		
	}
	public void actualizarListaLibros(int idLibro){
		ResultSet resultado = conexion.getInstance().consultar("EXEC generarReporte "+idLibro);
		if(resultado!=null){
			if(primeraVez)
				listaMasPretados.clear();
			try{
				while(resultado.next()){
					Libro libro=new Libro();
					libro.setIdLibro(resultado.getInt("idLibro"));				
					libro.setIdCategoria(resultado.getInt("idCategoria"));
					libro.setIdEditorial(resultado.getInt("idEditorial"));
					libro.setNombreLibro(resultado.getString("nombreLibro"));
					libro.setEdicion(resultado.getInt("edicion"));
					libro.setPaginasLibro(resultado.getInt("paginasLibro"));
					libro.setNombreEditorial(resultado.getString("nombreEditorial"));
					libro.setNombreCategoria(resultado.getString("nombreCategoria"));
					listaMasPretados.add(libro);
				}
			}catch(SQLException sql){
				sql.printStackTrace();
			}
		}		
	}
	public void actualizarListaN(String valor){
		ResultSet resultado = conexion.getInstance().consultar("EXEC listaDeLibroN '"+valor+"'");
		if(resultado!=null){
			listaDeLibrosN.clear();
			try{
				while(resultado.next()){
					Libro libro=new Libro();
					libro.setIdLibro(resultado.getInt("idLibro"));				
					libro.setIdCategoria(resultado.getInt("idCategoria"));
					libro.setIdEditorial(resultado.getInt("idEditorial"));
					libro.setNombreLibro(resultado.getString("nombreLibro"));
					libro.setEdicion(resultado.getInt("edicion"));
					libro.setPaginasLibro(resultado.getInt("paginasLibro"));
					libro.setNombreEditorial(resultado.getString("nombreEditorial"));
					libro.setNombreCategoria(resultado.getString("nombreCategoria"));
					listaDeLibrosN.add(libro);
				}
			}catch(SQLException sql){
				sql.printStackTrace();
			}
		}		
	}
	public ObservableList<Libro> getListaLibro(){
		actualizarLista();
		return listaDeLibros;
	}
	public ObservableList<Libro> getListaLibroN(String valor){
		actualizarListaN(valor);
		return listaDeLibrosN;
	}
	public ObservableList<Libro> getListaLibroValor(String valor){
		actualizarListaValor(valor);
		return listaDeLibrosValor;
	}
	public ObservableList<Libro> getListaLibroLibro(ArrayList<Prestamo> listaP){
		primeraVez=true;
		for(Prestamo prest:listaP){
			primeraVez=false;
			actualizarListaLibros(prest.getIdLibro());
		}
		return listaMasPretados;
	}
	public int lastId(){
		int id=0;
		actualizarLista();
		for(Libro libro:listaDeLibros){
			id=libro.getIdLibro();
		}
		return id;
	}
	public void eliminarLibro(int id){
		conexion.getInstance().ejecutar("EXEC eliminarLibro "+id+"");
		actualizarLista();
	}
	public void agregarLibro(Libro libro){
		conexion.getInstance().ejecutar("EXEC agregarLibro "+libro.getIdCategoria()+","+libro.getIdEditorial()+",'"+libro.getNombreLibro()+"',"+libro.getEdicion()+","+libro.getPaginasLibro()+"");
		actualizarLista();
	}
}

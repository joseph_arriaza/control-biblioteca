package org.carloscarrera.manejadores;

import java.sql.SQLException;
import java.sql.ResultSet;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import javafx.beans.property.SimpleListProperty;
import java.util.ArrayList;
import org.carloscarrera.utilidad.Fecha;
import org.carloscarrera.manejadores.ManejadorLibro;
import org.carloscarrera.conexion.Conexion;
import org.carloscarrera.bean.Prestamo;
import org.carloscarrera.bean.Libro;
public class ManejadorPrestamo{
	private Conexion conexion;
	private int idUser;
	private ObservableList<Prestamo> listaDePrestamos;
	private ObservableList<Prestamo> listaDeMisPrestamos;
	private ObservableList<Prestamo> listaTop10;
	private ObservableList<Libro> listaMasPretados;
	private ManejadorLibro mLibro;
	public ManejadorPrestamo(Conexion conexion){
		this.listaDePrestamos = FXCollections.observableArrayList();
		this.listaDeMisPrestamos=FXCollections.observableArrayList();
		this.listaTop10=FXCollections.observableArrayList();
		this.conexion = conexion;
		actualizarLista();
	}
	public void actualizarLista(){
		ResultSet resultado = conexion.getInstance().consultar("EXEC listaPrestamo");
		if(resultado!=null){
			listaDePrestamos.clear();
			try{
				while(resultado.next()){
					int cost=resultado.getInt("costo");
					if(resultado.getString("estado").equalsIgnoreCase("pendiente")){
						cost=new Fecha().verificarCostos(resultado.getString("fecha"),resultado.getInt("costo"));
						Prestamo prestamo=new Prestamo(resultado.getInt("idPrestamo"),resultado.getInt("idLibro"),resultado.getInt("idUsuario"),cost,resultado.getString("nombre"),resultado.getString("fecha"),resultado.getString("nombreLibro"),resultado.getString("estado"),resultado.getString("fechaEntrega"));
						listaDePrestamos.add(prestamo);
					}					
				}
			}catch(SQLException sql){
				sql.printStackTrace();
			}
		}		
	}
	public void actualizarMiLista(int idUsuario){
		ResultSet resultado = conexion.getInstance().consultar("EXEC actualizarMiLista "+idUsuario);
		if(resultado!=null){
			listaDeMisPrestamos.clear();
			try{
				while(resultado.next()){
					int cost=resultado.getInt("costo");
					if(resultado.getString("estado").equalsIgnoreCase("pendiente"))
						cost=new Fecha().verificarCostos(resultado.getString("fecha"),resultado.getInt("costo"));
					else
						cost=new Fecha().verificarCostosEntregado(resultado.getString("fecha"),resultado.getString("fechaEntrega"),resultado.getInt("costo"));
					Prestamo prestamo=new Prestamo(resultado.getInt("idPrestamo"),resultado.getInt("idLibro"),resultado.getInt("idUsuario"),cost,resultado.getString("nombre"),resultado.getString("fecha"),resultado.getString("nombreLibro"),resultado.getString("estado"),resultado.getString("fechaEntrega"));
					listaDeMisPrestamos.add(prestamo);
				}
			}catch(SQLException sql){
				sql.printStackTrace();
			}
		}		
	}
	public void actualizarLista10(){
		ResultSet resultado = conexion.getInstance().consultar("EXEC Reporte");
		if(resultado!=null){
			listaTop10.clear();
			try{
				while(resultado.next()){
					Prestamo prestamo=new Prestamo();
					prestamo.setIdLibro(resultado.getInt("idLibro"));
					listaTop10.add(prestamo);
				}
			}catch(SQLException sql){
				sql.printStackTrace();
			}
		}		
	}
	public ObservableList<Prestamo> getListaPrestamo(){
		actualizarLista();
		return listaDePrestamos;
	}
	public ObservableList<Prestamo> getMiListaPrestamo(int idUsuario){
		idUser=idUsuario;
		actualizarMiLista(idUser);
		return listaDeMisPrestamos;
	}
	public ObservableList<Libro> getListaPrestamo10(){
		actualizarLista10();
		ArrayList<Prestamo> lista10=new ArrayList<Prestamo>(listaTop10);
		mLibro=new ManejadorLibro(conexion);
		listaMasPretados=mLibro.getListaLibroLibro(lista10);
		return listaMasPretados;
	}
	public void actualizarPrestamo(int idP){
		conexion.getInstance().ejecutar("EXEC actualizarPrestamo "+idP+",'"+Fecha.fechaHoy()+"'");
		actualizarLista();
		actualizarMiLista(idUser);		
	}
	public void agregarPrestamo(int idUsuario,int idLibro,String fecha){
		conexion.getInstance().ejecutar("EXEC agregarPrestamo "+idUsuario+","+idLibro+",'"+fecha+"'");
		idUser=idUsuario;
		actualizarLista();
		actualizarMiLista(idUser);
	}
}

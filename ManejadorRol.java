package org.carloscarrera.manejadores;

import java.sql.SQLException;
import java.sql.ResultSet;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import javafx.beans.property.SimpleListProperty;

import org.carloscarrera.conexion.Conexion;
import org.carloscarrera.bean.Rol;
public class ManejadorRol{
	private Conexion conexion;
	private ObservableList<Rol> listaRol;
	public ManejadorRol(Conexion conexion){
		this.listaRol = FXCollections.observableArrayList();
		this.conexion = conexion;
	}
	public void actualizarLista(){
		ResultSet resultado = conexion.getInstance().consultar("EXEC listaRol");
		if(resultado!=null){
			listaRol.clear();
			try{
				while(resultado.next()){
					Rol rol=new Rol();
					rol.setIdRol(resultado.getInt("idRol"));				
					rol.setNombre(resultado.getString("nombreRol"));
					listaRol.add(rol);
				}
			}catch(SQLException sql){
				sql.printStackTrace();
			}
		}		
	}
	public ObservableList<Rol> getListaRol(){
		actualizarLista();
		return listaRol;
	}
}

package org.carloscarrera.manejadores;

import java.sql.SQLException;
import java.sql.ResultSet;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import javafx.beans.property.SimpleListProperty;

import org.carloscarrera.conexion.Conexion;
import org.carloscarrera.bean.SubCategoria;
public class ManejadorSubCategoria{
	private Conexion conexion;
	private ObservableList<SubCategoria> listaDeSubCategorias;
	private ObservableList<SubCategoria> listaDeSubCategoriasN;
	public ManejadorSubCategoria(Conexion conexion){
		this.listaDeSubCategorias = FXCollections.observableArrayList();
		this.listaDeSubCategoriasN = FXCollections.observableArrayList();
		this.conexion = conexion;
		actualizarLista();
	}
	public void actualizarLista(){
		ResultSet resultado = conexion.getInstance().consultar("EXEC listaSubCategoria");
		if(resultado!=null){
			listaDeSubCategorias.clear();
			try{
				while(resultado.next()){
					SubCategoria subCategoria=new SubCategoria(resultado.getInt("idSubCategoria"),resultado.getString("nombre"));
					listaDeSubCategorias.add(subCategoria);
				}
			}catch(SQLException sql){
				sql.printStackTrace();
			}
		}		
	}
	public void actualizarListaN(String valor){
		ResultSet resultado = conexion.getInstance().consultar("EXEC listaSubCategoriaN '"+valor+"'");
		if(resultado!=null){
			listaDeSubCategoriasN.clear();
			try{
				while(resultado.next()){
					SubCategoria subCategoria=new SubCategoria(resultado.getInt("idSubCategoria"),resultado.getString("nombre"));
					listaDeSubCategoriasN.add(subCategoria);
				}
			}catch(SQLException sql){
				sql.printStackTrace();
			}
		}		
	}
	public ObservableList<SubCategoria> getListaSubCategoria(){
		actualizarLista();
		return listaDeSubCategorias;
	}
	public ObservableList<SubCategoria> getListaSubCategoriaN(String valor){
		actualizarListaN(valor);
		return listaDeSubCategoriasN;
	}
	public void actualizarSubCategoria(int id,String valor){
		conexion.getInstance().ejecutar("EXEC actualizarSubCategoria "+id+",'"+valor+"'");
		actualizarLista();
	}
	public void eliminarSubCategoria(int id){
		conexion.getInstance().ejecutar("EXEC eliminarSubCategoria "+id+"");
		actualizarLista();
	}
	public void agregarSubCategoria(String nombre){
		conexion.getInstance().ejecutar("EXEC agregarSubCategoria '"+nombre+"'");
		actualizarLista();
	}
}

package org.carloscarrera.manejadores;

import java.sql.SQLException;
import java.sql.ResultSet;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import javafx.beans.property.SimpleListProperty;

import org.carloscarrera.conexion.Conexion;
import org.carloscarrera.bean.Usuario;
public class ManejadorUsuario{
	private Usuario usuarioConectado;
	private Conexion conexion;
	private ObservableList<Usuario> listaDeUsuarios;
	private ObservableList<Usuario> listaDeUsuariosN;
	public ManejadorUsuario(Conexion conexion){
		this.listaDeUsuarios = FXCollections.observableArrayList();
		this.listaDeUsuariosN= FXCollections.observableArrayList();
		this.conexion = conexion;
	}
	public void actualizarLista(){
		if(usuarioConectado!=null){
			ResultSet resultado = conexion.getInstance().consultar("EXEC listaDeUsuarios");
			if(resultado!=null){
				listaDeUsuarios.clear();
				try{
					while(resultado.next()){
						Usuario user=new Usuario();
						user.setIdUsuario(resultado.getInt("idUsuario"));
						user.setIdRol(resultado.getInt("idRol"));					
						user.setNombreUsuario(resultado.getString("nombre"));
						user.setContrasenia(resultado.getString("contrasenia"));
						user.setNombreRol(resultado.getString("nombreRol"));
						listaDeUsuarios.add(user);
					}
				}catch(SQLException sql){
					sql.printStackTrace();
				}
			}
		}		
	}
	public ObservableList<Usuario> getListaUsuarios(){
		actualizarLista();
		return listaDeUsuarios;
	}
	public void actualizarListaN(String valor){
		if(usuarioConectado!=null){
			ResultSet resultado = conexion.getInstance().consultar("EXEC listaDeUsuariosN '"+valor+"'");
			if(resultado!=null){
				listaDeUsuariosN.clear();
				try{
					while(resultado.next()){
						Usuario user=new Usuario();
						user.setIdUsuario(resultado.getInt("idUsuario"));
						user.setIdRol(resultado.getInt("idRol"));					
						user.setNombreUsuario(resultado.getString("nombre"));
						user.setContrasenia(resultado.getString("contrasenia"));
						user.setNombreRol(resultado.getString("nombreRol"));
						listaDeUsuariosN.add(user);
					}
				}catch(SQLException sql){
					sql.printStackTrace();
				}
			}
		}		
	}
	public ObservableList<Usuario> getListaUsuariosN(String valor){
		actualizarListaN(valor);
		return listaDeUsuariosN;
	}
	public boolean autenticar(String username, String pass, int idioma){
		if(usuarioConectado!=null)
			return true;
		ResultSet resultado = conexion.getInstance().consultar("EXEC autenticar '"+username+"','"+pass+"'");
		try{
			if(resultado!=null){
				if(resultado.next()){
					Usuario user=new Usuario(resultado.getInt("idUsuario"),resultado.getInt("idRol"),resultado.getString("nombre"), resultado.getString("nombreRol"),resultado.getString("contrasenia"),idioma);
					usuarioConectado=user;
				}
			}
		}catch(SQLException sql){
			sql.printStackTrace();
		}
		return usuarioConectado!=null;
	}
	public void desautenticar(){
		usuarioConectado = null;
	}
	public Usuario getUsuarioAutenticado(){
		return this.usuarioConectado;
	}
	public void agregarUsuario(Usuario user){
		conexion.getInstance().ejecutar("addUser '"+user.getNombreUsuario()+"','"+user.getContrasenia()+"',"+user.getIdRol());
		actualizarLista();
	}
	public void actualizarUsuario(String valor,int idUsuario){
		conexion.getInstance().ejecutar("UPDATE Usuario SET contrasenia='"+valor+"' WHERE idUsuario="+idUsuario);
		actualizarLista();
	}	
	public void eliminarUsuario(int idUsuario){
		conexion.getInstance().ejecutar("EXEC eliminarUsuario "+idUsuario);
		actualizarLista();
	}
}

package org.carloscarrera.manejadores;

import java.sql.SQLException;
import java.sql.ResultSet;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import javafx.beans.property.SimpleListProperty;

import org.carloscarrera.conexion.Conexion;
import org.carloscarrera.bean.VarSubCategoria;
public class ManejadorVarSubCategoria{
	private Conexion conexion;
	private ObservableList<VarSubCategoria> listaDeVarSubCategorias;
	private ObservableList<VarSubCategoria> listaVarSubCategoriaLibro;
	public ManejadorVarSubCategoria(Conexion conexion){
		this.listaDeVarSubCategorias = FXCollections.observableArrayList();
		this.listaVarSubCategoriaLibro = FXCollections.observableArrayList();
		this.conexion = conexion;
		actualizarLista();
	}
	public void actualizarLista(){
		ResultSet resultado = conexion.getInstance().consultar("EXEC listaVarSubCategoria");
		if(resultado!=null){
			listaDeVarSubCategorias.clear();
			try{
				while(resultado.next()){
					VarSubCategoria varSubCategoria=new VarSubCategoria(resultado.getInt("idSubCategoria"),resultado.getInt("idLibro"),resultado.getString("nombreCat"),resultado.getString("nombreLibro"));
					listaDeVarSubCategorias.add(varSubCategoria);
				}
			}catch(SQLException sql){
				sql.printStackTrace();
			}
		}		
	}
	public void actualizarListaLibro(int idLibro){
		ResultSet resultado = conexion.getInstance().consultar("EXEC listaVarSubCategoriaL "+idLibro);
		if(resultado!=null){
			listaVarSubCategoriaLibro.clear();
			try{
				while(resultado.next()){
					VarSubCategoria varSubCategoria=new VarSubCategoria(resultado.getInt("idSubCategoria"),resultado.getInt("idLibro"),resultado.getString("nombreCat"),resultado.getString("nombreLibro"));
					listaVarSubCategoriaLibro.add(varSubCategoria);
				}
			}catch(SQLException sql){
				sql.printStackTrace();
			}
		}		
	}
	public ObservableList<VarSubCategoria> getListaVarSubCategoria(){
		actualizarLista();
		return listaDeVarSubCategorias;
	}
	public ObservableList<VarSubCategoria> getListaVarSubCategoriaLibro(int idLibro){
		actualizarListaLibro(idLibro);
		return listaVarSubCategoriaLibro;
	}
	public void agregarVarSubCategoria(int idLibro,int idSubCategoria){
		conexion.getInstance().ejecutar("EXEC agregarVarSubCategoria "+idLibro+","+idSubCategoria+" ");
		actualizarLista();
	}
}
